/*
 File:		BrowserFavoritesModel.h
 Author:	Serguei Kolos
 */

#ifndef OHPPLUGINS_BROWSER_FAVORITES_MODEL_H
#define OHPPLUGINS_BROWSER_FAVORITES_MODEL_H

#include <ohpplugins/BrowserTreeModel.h>

namespace ohpplugins
{
    class BrowserFavoritesModel : public BrowserTreeModel
    {
	Q_OBJECT

     public:
	BrowserFavoritesModel(QObject *parent = 0)
          : BrowserTreeModel(parent)
        { ; }
	
        void addItem(const QSharedPointer<BrowserTreeModel::Item>& item);
        
        void removeItems(const QModelIndexList& indexes);
        
     protected:
        QModelIndex parent(const QModelIndex& index) const;
    };
}

#endif
