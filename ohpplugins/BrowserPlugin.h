/*
 File:		BrowserPlugin.h
 Author:	andrea.dotti@cern.ch
 Modified:	Serguei Kolos
 */
#ifndef OHPPLUGINS_BROWSER_PLUGIN_H
#define OHPPLUGINS_BROWSER_PLUGIN_H

#include <string>

#include <QMenu>

#include <ohpplugins/common/PluginWidget.h>

#include <ohpplugins/BrowserTreeModel.h>
#include <ohpplugins/BrowserFavoritesModel.h>

#include <ui_BrowserPanel.h>

using namespace boost::multi_index;

namespace ohpplugins
{        
    class BrowserPlugin: public PluginWidget,
			 public Ui::BrowserPanel
    {
	Q_OBJECT
        
    public:
	BrowserPlugin(const std::string& name);

	void configure(const ohp::PluginInfo& config);
        
	void histogramUpdated(const QString& histogram_name);
        
    protected slots:
        void infrastructureUp();

        void serverUp(const QString& );
        
	void showContextMenu(const QPoint& );

        bool eventFilter(QObject *obj, QEvent *event);
                
        void drawHistograms();
        
        void drawHistogramsWithConfig();
        
        void updateContent();
        
        void refreshRequested();
        
        void addToFavorites();
        
        void removeFromFavorites();
        
    private:
        void drawHistograms(bool use_config);
        
        void histogramSourceChanged();
        
    private:
	BrowserTreeModel	m_tree_model;
	BrowserFavoritesModel	m_favorites_model;
        QMenu			m_tree_menu;
        QMenu			m_favorites_menu;
        QMenu			m_base_menu;
        bool			m_use_config;
        bool			m_content_up_to_date;
    };
}

#endif
