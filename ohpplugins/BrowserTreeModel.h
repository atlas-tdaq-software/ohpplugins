/*
 File:		BrowserTreeModel.h
 Author:	Serguei Kolos
 */

#ifndef OHPPLUGINS_BROWSER_TREE_MODEL_H
#define OHPPLUGINS_BROWSER_TREE_MODEL_H

#include <string>
#include <vector>

#include <QAbstractItemModel> 
#include <QSharedPointer> 
#include <QMap>
#include <QIcon>

class QTreeView;

namespace ohpplugins
{
    class BrowserTreeModel : public QAbstractItemModel
    {
	Q_OBJECT

    public:
	class Item
	{
	public:
	    Item(Item* parent,
		 const QString& name, 
		 const QString& server,
		 const QString& provider,
                 const QString& path,
		 const QIcon* icon,
                 unsigned int histograms_count = 0);
                 
	    void addChild(const QString& server, const QString& provider, const QString& name);
            
            void addChild(const QSharedPointer<Item>& child);
            
            void removeChild(const Item* child);
            
            bool hasChild(const QString& name) const { return (m_children.find(name) != m_children.end()); }
            
            Item* child(int row) const { return (m_children.begin() + row).value().data(); }
	    
            int childCount() const { return m_children.size(); }
	    
            int columnCount() const { return 2; }
            
	    const QString& name() const { return m_name; }
	    
	    const QIcon& icon() const { return *m_icon; }
            
	    unsigned int histogramCount() const { return m_histograms_count; }
            
            int row() const;
	    
            int adapteeRow() const;
            
            const Item* parent() const { return m_parent; }
            
            void clear();
                        
            QString getHistogramName() const { return m_server + '/' + m_provider + m_path; }
            
            const QString& getServer() const { return m_server; }
            
            const QString& getProvider() const { return m_provider; }
            
            const QString& getPath() const { return m_path; }
            
            QString getPathPattern() const { return m_path.isEmpty() ? ".*" : m_path + "/.*"; }
            
            void getSelectedHistograms(std::vector<std::string>& histograms) const;
                        
            const Item* stepparent() { return m_stepparent; }
	    
            QSharedPointer<Item> getChild(Item* item) const;
            
            bool inBookmarkedBranch() const;
            
            bool isBookmarked() const;
            
	private:
	    void addChild(const QString& provider, const QString& name);
	    
	    void addChild(const QString& name);
            
            void updateHistogramsCount(unsigned int count);

	private:
	    typedef QMap<QString, QSharedPointer<Item> >	Children;
            
	    Item*	 m_parent;
            Item*	 m_stepparent;
	    Children	 m_children;
	    QString	 m_name;
	    QString	 m_server;
	    QString	 m_provider;
	    QString	 m_path;
            unsigned int m_histograms_count;
            const QIcon* m_icon;
            int		 m_row;
	};

     public:
	BrowserTreeModel(QObject *parent = 0);
        
	std::vector<std::string> getSelectedHistograms(const QModelIndexList& indexes);
        	
        void reset();
        
        void refresh(QTreeView* tv);
        
        void serverUp(const QString& server);
        
        QSharedPointer<Item> getItem(const QModelIndexList& indexes) const;
        
     protected:
	QVariant data(const QModelIndex& index, int role) const;
        
	Qt::ItemFlags flags(const QModelIndex& index) const;
        
	QVariant headerData(int section, Qt::Orientation orientation,
			   int role = Qt::DisplayRole) const;
                            
	QModelIndex index(int row, int column,
			 const QModelIndex& parent = QModelIndex()) const;
                          
	QModelIndex parent(const QModelIndex& index) const;
        
	int rowCount(const QModelIndex& parent = QModelIndex()) const;
        
	int columnCount(const QModelIndex& parent = QModelIndex()) const;
        
     protected:        
        void addServer(const QString& server, const QString& provider, const QString& histogram);

        static QIcon* s_node_icon;
        static QIcon* s_node_star_icon;
        static QIcon* s_leaf_icon;
        static QIcon* s_leaf_star_icon;
        
	Item  m_root;
    };
}

#endif
