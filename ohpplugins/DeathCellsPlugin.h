/*
  File:		DeathCellsPlugin.h
  Author:	Roberto Agostino Vitillo [vitillo@cern.ch]
  Modified:	Serguei Kolos
*/

#ifndef OHPPLUGIN_DEATHCELLS_PLUGIN_H
#define OHPPLUGIN_DEATHCELLS_PLUGIN_H

#include <ohpplugins/common/TileHelper.h>
#include <ohpplugins/common/TableModel.h>

namespace ohpplugins
{
    class DeathCellsPlugin : public TableModel
    {
    public:
	enum Columns { MODULE = 0, CHANNEL };
        
	DeathCellsPlugin();

	void configure(const ohp::PluginInfo& config, std::vector<std::string>& histograms);

	void histogramUpdated(const std::string& name, bool show_masked);

      private:
        TileHelper	m_tile_helper;
    };
}

#endif
