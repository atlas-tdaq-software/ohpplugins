/*
  File:		ErrorsPlugin.h
  Author:	Roberto Agostino Vitillo [vitillo@cern.ch]
  Modified:	Serguei Kolos
*/

#ifndef OHPPLUGIN_ERRORS_PLUGIN_H
#define OHPPLUGIN_ERRORS_PLUGIN_H

#include <ohpplugins/common/TileHelper.h>
#include <ohpplugins/common/TableModel.h>

namespace ohpplugins
{
    class ErrorsPlugin : public TableModel
    {
      public:
	enum Columns {
	    MODULE = 0, DMU, BCIDERR, DMUERR,
	    NUMEVENTS, HEADPARITY, HEADERFORMAT,
	    DOUBLESTROBE, SINGLESTROBE,
	    MEMORYPARITYERROR
	};

	ErrorsPlugin();

	void configure(const ohp::PluginInfo& config, std::vector<std::string>& histograms);

	void histogramUpdated(const std::string& name, bool show_masked);

      private:
        TileHelper	m_tile_helper;
    };
}

#endif
