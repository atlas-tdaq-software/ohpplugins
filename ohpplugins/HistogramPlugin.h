/*
 File:		HistogramPlugin.h
 Author:	andrea.dotti@cern.ch
 Modified:	Serguei Kolos
 */
#ifndef OHPPLUGINS_HISTOGRAM_PLUGIN_H
#define OHPPLUGINS_HISTOGRAM_PLUGIN_H

#include <string>

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/scoped_ptr.hpp>

#include <ohp/PluginBase.h>

#include <ohpplugins/common/PluginWidget.h>

#include <ui_HistogramPanel.h>

using namespace boost::multi_index;

namespace ohpplugins
{        
    /** This class implements canvas that displays histograms. */
    class HistogramPlugin: public PluginWidget,
    			   public Ui::HistogramPanel
    {
	Q_OBJECT
                
    public:
	HistogramPlugin(const std::string& name);

	void configure(const ohp::PluginInfo& config);
        
    public slots:
	void histogramUpdated(const QString& name);
        
        void resumed();
			
	void updateContent();

    protected:                
	/** Draw histogram identified by @param histogram_name */
	virtual void drawHistogram(const std::string& histogram_name);

    private:        
        struct Histogram
        {
	    Histogram(const std::string& n, unsigned int id)
	      : m_name(n),
                m_pad_id(id)
	    { ; }

	    const std::string	m_name;
	    const unsigned int	m_pad_id;
        };

        typedef boost::multi_index_container<
	    Histogram,
	    indexed_by<
		hashed_non_unique <
		    member<Histogram,const std::string,&Histogram::m_name>
		>,
		ordered_non_unique <
		    member<Histogram,const unsigned int,&Histogram::m_pad_id>
		>
	    >
	> Histograms;

        typedef Histograms::nth_index<1>::type HistogramsPadOrdered;

    private:
        unsigned int	m_nx;
        unsigned int	m_ny;
	Histograms	m_histograms;
    };
}

#endif
