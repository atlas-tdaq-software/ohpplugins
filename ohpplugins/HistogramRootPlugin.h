/*
  File:		HistogramRootPlugin.h
  Author:	andrea.dotti@cern.ch, tomasz.bold@cern.ch
  Modified:	Serguei Kolos
 */
#ifndef OHPPLUGINS_HISTOGRAM_ROOT_PLUGIN_H
#define OHPPLUGINS_HISTOGRAM_ROOT_PLUGIN_H

#include <memory>

#include <QElapsedTimer>

#include <ohpplugins/common/PluginWidget.h>

#include <ui_HistogramPanel.h>

class RootPlugin;

namespace ohpplugins
{
  class HistoWindowRP_impl;

  class HistogramRootPlugin :	public PluginWidget,
				public Ui::HistogramPanel
  {
    public:
      HistogramRootPlugin(const std::string& name);

    protected:
      void configure(const ohp::PluginInfo& config);
      
      void histogramUpdated(const QString& name);
      
      void updateContent();

    private:
      bool createRootPlugin(	const std::string& className, 
				const std::string& library, 
                                const std::string& constructor);
      
      void update();
      
    private:
      int				m_timeout;
      bool				m_require_all_histograms;
      std::unique_ptr<RootPlugin>	m_root_plugin;
      QElapsedTimer		        m_timer;
  };
}


#endif
