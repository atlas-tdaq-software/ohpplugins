/*
  File:		HistoWindowTabPlugin.h
  Author:	andrea.dotti@cern.ch
  MODIFIED:	Roberto Agostino Vitillo [vitillo@cli.di.unipi.it] (Mid 2008) -- MDI support added
  MODIFIED:	Serguei Kolos
*/
#ifndef OHPPLUGINS_HISTOGRAM_TAB_PLUGIN_H
#define OHPPLUGINS_HISTOGRAM_TAB_PLUGIN_H

#include <vector>
#include <boost/shared_ptr.hpp>

#include <ohpplugins/HistogramPlugin.h>

#include <ui_HistogramTabPanel.h>

namespace ohpplugins 
{
    class HistogramTabPlugin :	public PluginWidget,
				public Ui::HistogramTabPanel
    {
	Q_OBJECT
        
    public:
	HistogramTabPlugin(const std::string& name);
      
    protected:
	void configure(const ohp::PluginInfo& config);
	void resumed();
	void updateContent();
	void setVisible(bool visible);

    protected:
        typedef std::vector<boost::shared_ptr<EventReceiver> >	Tabs;
        
        Tabs	m_tabs;
    };
}

#endif
