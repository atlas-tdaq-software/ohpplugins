/*
  File:		MDI.cpp
  Author:	Roberto Agostino Vitillo [vitillo@cli.di.unipi.it]
  Modified:	Serguei Kolos
*/

#ifndef OHPPLUGINS_MDI_H
#define OHPPLUGINS_MDI_H

#include <string>
#include <map>

#include <TFile.h>
#include <TDirectory.h>

#include <QDebug>
#include <QString>
#include <QToolBox>
#include <QSpinBox>
#include <QPushButton>
#include <QMouseEvent>
#include <QMainWindow>
#include <QMdiSubWindow>
#include <QLineEdit>
#include <QSize>
#include <QWidgetAction>

#include <ui_MDIPanel.h>

#include <ohp/PluginBase.h>
#include <ohp/ArchiveHistogramManager.h>
#include <ohpplugins/common/PluginWidget.h>

namespace ohpplugins 
{    
    class CheckBox : public QPushButton
    {
	Q_OBJECT
      public:
	CheckBox(QWidget* parent, ohp::PluginBase* plugin, 
			QLayout * window, QLayout* tab);
	
	void showInTab()
	{
	    m_layout->removeWidget(m_plugin);
	    m_plugin->setVisible(true);
	    m_tab->addWidget(m_plugin);
	}

	void showInWindow()
	{
	    m_tab->removeWidget(m_plugin);
	    m_layout->addWidget(m_plugin);
	    setPluginVisible(m_visibility);
	}
        
      private slots:
        void windowClosed()
        { setChecked(false); }

	void setPluginVisible(bool visible) {
	    m_layout->parentWidget()->setVisible(visible); 
            m_plugin->setVisible(visible);
            m_visibility = visible;
	}

      private:
	void mousePressEvent(QMouseEvent* e) {
	    if(e->button() == Qt::RightButton && isChecked()) {
            	m_layout->parentWidget()->setFocus();
	    }
	    return QPushButton::mousePressEvent(e);
	}
        
      private:
        QWidget* m_plugin;
        QLayout* m_layout;
        QLayout* m_tab;
        bool	 m_visibility;
    };
    
    class SpinBox : public QSpinBox
    {
	Q_OBJECT
      public:
    	SpinBox(const std::set<daq::mda::RunInfo> & values)
          : m_values(values),
            m_pos(0)
        {
            lineEdit()->setReadOnly(true);
            setPrefix("Run: ");
            if ( !m_values.empty() ) {
            	setRange(m_values.begin()->run(), m_values.rbegin()->run());
                stepBy(m_values.size());
            }
            else {
            	setRange(0, 0);
            }
        }
        
        QSize sizeHint () const
        {
            QSize s = QSpinBox::sizeHint();
            s.setWidth(s.width() * 2);
            return s;
        }
        
      protected:
        void stepBy( int steps );
        
      private:
        std::set<daq::mda::RunInfo>	m_values;
        int				m_pos;
    };
    
    class RunNumberSelector: public QWidgetAction
    {
	Q_OBJECT
        
      signals:
	void runNumberSelected(unsigned int , const QString&);
      
      public:
        RunNumberSelector(QWidget * parent, const std::set<daq::mda::RunInfo> & values,
        		  const QObject * receiver, const char * member)
          : QWidgetAction(parent)
        {
            QHBoxLayout * layout = new QHBoxLayout();
            layout->setContentsMargins(0, 0, 0, 0);
            
            m_spin = new SpinBox(values);
            layout->addWidget(m_spin);

            QPushButton * ok = new QPushButton(QIcon(":/images/accept.png"), "");
            ok->setIconSize(QSize(20,20));
            layout->addWidget(ok);

            QFrame * frame = new QFrame();
	    frame->setLayout(layout);
	    setDefaultWidget(frame);

            connect(ok, SIGNAL(released()), this, SLOT(buttonReleased()));
            connect(this, SIGNAL(runNumberSelected(unsigned int , const QString& )),
                    receiver, member, Qt::QueuedConnection);
	}
        
      private slots:        
        void buttonReleased()
        {
            ((QMenu*)parentWidget())->hide();
            emit runNumberSelected(m_spin->value(), m_spin->suffix());
        }
        
      private:
        SpinBox * m_spin;
    };

    class MDIPlugin :	public QMainWindow,
    			public ohp::PluginBase,
    			public Ui::MDIPanel
    {
	Q_OBJECT
        
	bool eventFilter(QObject* o, QEvent* e)
        {
            if ( e->type() == QEvent::MouseButtonPress )
            {
            	QAction* a = ((QMenu*)o)->actionAt(((QMouseEvent*)e)->pos());
                if ( a && a != actionInputOnlineSystem )
                    return true;
            }
            return QObject::eventFilter(o, e);
        }
        
    public:
	MDIPlugin(const std::string& name);
        
	void configure(const ohp::PluginInfo& config);	
        
    signals:        
	void windowClosed();

    public slots:        
	void on_actionOpen_triggered();
        
	void on_actionSaveAs_triggered();
        
	void on_actionPause_toggled(bool state);

	void on_actionStart_toggled(bool state);

	void on_actionViewAsWindows_toggled(bool state);

	void on_actionViewAsTabs_toggled(bool state);

	void on_actionAbout_triggered();
        
	void on_actionIncreaseFontSize_triggered();
        
	void on_actionDecreaseFontSize_triggered();

	void on_actionRefresh_triggered();
        
        void inputRunNumberSelected(unsigned int run_number, const QString & time);

        void inputFileToggled();

        void on_actionInputOnlineSystem_toggled(bool state);
        
    protected:
	void saveState(QSettings& settings);
        
	void restoreState(const QSettings& settings);
        
	void aboutToDisplay( );

        void closeEvent(QCloseEvent* e);
        
    private:
	TDirectory* createTDirectory(const std::string& path, TFile* file);
        
	void writeToFile(const std::string & filename);
        
        void updateFontSize();
        
    private:
	typedef std::vector<boost::shared_ptr<ohp::PluginBase> > Plugins;
	typedef std::vector<CheckBox*> 				 Buttons;
        typedef boost::shared_ptr<ohp::ArchiveHistogramManager>  ArchiveManager;
        
	Plugins		m_plugins;
	Buttons		m_buttons;
        unsigned int	m_font_size;
        ArchiveManager	m_archive;
        QActionGroup	m_actions_group;
        QActionGroup	m_sources_group;
        QActionGroup	m_files_group;
    };
}

#endif
