/*
  File:	        PixelTabMapPlugin.h
  Author:	Giulia Ucchielli
  Modified:	Antonio De Maria, Nanjing University, antonio.de.maria@cern.ch
*/

#ifndef OHPPLUGIN_PLUGIN_H
#define OHPPLUGIN_PLUGIN_H

#include <ohpplugins/common/TableModel.h>
#include <ohpplugins/common/TablePanel.h>

namespace ohpplugins
{
    class PixelTabMapPlugin : public TableModel
    {        
    public:

	PixelTabMapPlugin();

	void configure(const ohp::PluginInfo& config, std::vector<std::string>& histograms);

	void histogramUpdated(const std::string& name, bool show_masked);
	
	//std::vector<QVariant> manipulateVerticalHeader(std::vector<std::string> lineArray); !<< not used

    protected:
        double		m_threshold;
        int		m_table_row;
        int		m_num_layers;
	std::map<std::string ,std::vector<std::string> > m_layers;
        const char*     m_layer_name[9]= {"ALL","IBL","IBLA","IBLC","L0","L1","L2","ECA","ECC"};
        std::map<std::string,QColor> m_dqmf_result;
        //std::vector<QVariant> m_tableLines; !<< not used 

    };
}

#endif
