/*
  File:		RunStatusPlugin.h
  Author:	andrea.dotti@cern.ch
  MODIFIED:	Roberto Agostino Vitillo [vitillo@cli.di.unipi.it] (Mid 2008) -- MDI support added
  MODIFIED:	Serguei Kolos
 */
#ifndef OHPPLUGINS_RUN_STATUSPLUGIN_H
#define OHPPLUGINS_RUN_STATUSPLUGIN_H

#include <string>
#include <vector>

#include <boost/scoped_ptr.hpp>

#include <QMap>
#include <QColor>
#include <QString>

#include <ipc/partition.h>
#include <is/inforeceiver.h>
#include <ohpplugins/common/PluginWidget.h>

#include <ui_RunStatusPanel.h>

namespace ohpplugins
{
    /** Widget implementation for the status_panel Plugin.
	this class implements the main widget for the status plugin.
     */
    class RunStatusPlugin :	public PluginWidget,
    				public Ui::RunStatusPanel
    {
	Q_OBJECT
        
    signals:
	void runNumber(int);
        void lbNumber(int);
	void runType(const QString& );
	void runDate(const QString& );
	void runState(const QString& );
        
    public:
	RunStatusPlugin(const std::string& name);
              
    public slots:
	void setRunNumber(int);
        void setLumiBlockNumber(int);
	void setRunType(const QString& s);
	void setRunDate(const QString& d);
	void setRunState(const QString& d);
        
    protected slots:
        void infrastructureUp();
            
    private:
	void runParamsReceiver(ISCallbackInfo * info);
        void lumiBlockReceiver(ISCallbackInfo * info);
	void runControlReceiver(ISCallbackInfo * info);
      
    private:
	QMap<QString,QString>			m_state_colors;
        boost::scoped_ptr<ISInfoReceiver>	m_receiver;
    };
}

#endif
