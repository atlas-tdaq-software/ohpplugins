/*
  File:		StatusPlugin.h
  Author:	andrea.dotti@cern.ch
  MODIFIED:	Roberto Agostino Vitillo [vitillo@cli.di.unipi.it] (Mid 2008) -- MDI support added
  MODIFIED:	Serguei Kolos
 */
#ifndef OHPPLUGINS_STATUSPLUGIN_H
#define OHPPLUGINS_STATUSPLUGIN_H

#include <string>
#include <vector>

#include <boost/scoped_ptr.hpp>

#include <QTimer>
#include <QPlainTextEdit>

#include <ohpplugins/common/PluginWidget.h>

#include <ui_StatusPanel.h>

namespace ohpplugins
{
    /** Widget implementation for the status_panel Plugin.
	this class implements the main widget for the status plugin.
     */
    class StatusPlugin : public PluginWidget,
    			 public Ui::StatusPanel
    {
	Q_OBJECT
        
    public slots:
	void updateRates();
      
    protected slots:
        void serverUp(const QString& msg);
	void serverDown(const QString& msg);

	void paused();
	void resumed();
    
    public:
	StatusPlugin(const std::string& name);
        
    private:
	void updateServers(const std::vector<std::string> & servers, QLabel* widget);
      
    private:
	std::vector<std::string>	m_online_servers;
	std::vector<std::string>	m_offline_servers;
        int				m_last_input_num;
        QTimer				m_timer;
    };
}

#endif
