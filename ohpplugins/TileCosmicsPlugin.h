/*
 File:		TileCocmicsPlugin.h
 Author:	andrea.dotti@cern.ch
 Modified:	Serguei Kolos
 */
#ifndef OHPPLUGINS_TILE_COSMICS_PLUGIN_H
#define OHPPLUGINS_TILE_COSMICS_PLUGIN_H

#include <list>
#include <vector>
#include <string>
#include <map>

#include <ohp/PluginBase.h>

#include <ohpplugins/common/PluginWidget.h>

#include <ui_TileCosmicsPanel.h>

class TH1;

namespace ohpplugins
{        
    /** This class implements canvas that displays histograms. */
    class TileCosmicsPlugin:	public PluginWidget,
    				public Ui::TileCosmicsPanel
    {
	Q_OBJECT
        
    public:
	TileCosmicsPlugin(const std::string& name);

	void configure(const ohp::PluginInfo& config);
        
    public slots:        
    	void on_autoDisplay_toggled(bool state);

    	void on_showFirst_toggled(bool state);

    	void on_showLast_toggled(bool state);

	void histogramUpdated(const QString& name);
        
        void resumed();
			
	void updateContent();

    protected:                
	void setVisisble(bool state);
        
    private:                
        void setOrdering(const std::string& type);
        void drawList(const std::list<TH1*>& histograms);
        void setDefaults();

    private:        
	typedef boost::function<bool (TH1* const &, TH1* const &)>	Ordering;
	typedef std::vector<std::string> 				Modules;
	typedef std::map<std::string, std::string>			ProvidersMap;
        
    private:
	std::string	m_template_name;
        Ordering	m_ordering;
        Modules		m_modules;
        ProvidersMap	m_providers;
        int		m_events;
    };
}

#endif
