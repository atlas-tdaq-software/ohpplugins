/*
  File:		TileNoisePlugin.h
  Author:	Roberto Agostino Vitillo [vitillo@cern.ch]
  Modified:	Serguei Kolos
*/

#ifndef OHPPLUGIN_HOTCELLS_PLUGIN_H
#define OHPPLUGIN_HOTCELLS_PLUGIN_H

#include <ohpplugins/common/TileHelper.h>
#include <ohpplugins/common/TableModel.h>

namespace ohpplugins
{
    class TileNoisePlugin : public TableModel
    {        
    public:
	enum Columns { MODULE = 0, PMT, VALUE };

	TileNoisePlugin();

	void configure(const ohp::PluginInfo& config, std::vector<std::string>& histograms);

	void histogramUpdated(const std::string& name, bool show_masked);

    protected:
        double		m_threshold;
        TileHelper	m_tile_helper;
    };
}

#endif
