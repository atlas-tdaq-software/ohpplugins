/*
  File:		CommandDialog.h
  Author:	andrea.dotti@cern.ch
  Modified:	Serguei Kolos
 */

#ifndef OHPPLUGINS_COMMANDDIALOG_H
#define OHPPLUGINS_COMMANDDIALOG_H

#include <ui_CommandDialog.h>

#include <QObject>
#include <QString>
#include <QWidget>
#include <QGroupBox>

namespace ohpplugins
{
    class CommandDialog : public QDialog,
    			  public Ui::CommandDialog
    {
	Q_OBJECT 
        
    public:
	CommandDialog(QWidget* parent=0);
      
	void setHistogramName(const QString& histogram_name);
      
    public slots:
	void on_sendButton_clicked();
	void on_histogramRadioButton_toggled(bool state);
	void on_providerRadioButton_toggled(bool state);
      
    signals:
	void commandSent(const QString& histogram_name, const QString& command);
    
    private:
    	QString	m_server_provider;
    	QString	m_full_name;
    };
}
#endif
