/*
  File: HltCanvasWatcher.h
 */

#ifndef OHPPLUGINS_HLTCANVASWATCHER_H
#define OHPPLUGINS_HLTCANVASWATCHER_H

#include <ohpplugins/common/TQCanvasWatcher.h>

class TH2F;
class QEvent;
class QMouseEvent;

namespace ohpplugins
{
    class HltCanvasWatcher : public TQCanvasWatcher
    {
      Q_OBJECT 
    public:
      HltCanvasWatcher(QObject * parent);
      
    protected:
      virtual bool mouseHandler(QObject* obj, QMouseEvent* evt);

    private:
      bool processHistogram(TH2F* tobj, float x, float y);
    };
}
#endif
