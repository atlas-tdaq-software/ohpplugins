/*
  FILE:         PluginWidget.h
  AUTHOR:	Serguei Kolos
*/

#ifndef OHPPLUGINS_PLUGIN_WIDGET_H
#define OHPPLUGINS_PLUGIN_WIDGET_H

#include <iostream>
#include <string>
#include <vector>

#include <QApplication>
#include <QDebug>
#include <QEvent>
#include <QCloseEvent>
#include <QSettings>
#include <QWidget>
#include <QMdiSubWindow>

#include <ohp/PluginBase.h>
#include <ohpplugins/common/StatusBar.h>


namespace ohpplugins 
{
    typedef std::vector<std::string>  HistogramsList;
    
    struct EventReceiver
    {
	virtual void histogramUpdated(const QString& ) { ; }

	virtual void paused() { ; }

	virtual void resumed() { ; }

	virtual void infrastructureUp() { ; }

	virtual void infrastructureDown() { ; }

	virtual void serverUp(const QString& ) { ; }

	virtual void serverDown(const QString& ) { ; }
    };
    
    class PluginWidget : public QWidget,
    			 public EventReceiver,
    			 public ohp::PluginBase
    {
	Q_OBJECT
        
      public:
	virtual ~PluginWidget() { ; }

	const HistogramsList& getWatchedHistograms() const { return m_histograms; }
 
 	void setWatchedHistograms(const HistogramsList& histograms) 
        { 
            m_histograms = histograms;
            if (this->isVisible()) {
		subscribe();
		subscribe(m_histograms);
            }
        }
 	
        virtual void windowActivated();

      signals:
	virtual void windowClosed();

      private slots:
	void subWindowActivated(QMdiSubWindow* sw);
        
      protected:
	void saveState(QSettings& settings) 
        {
            QWidget* w = static_cast<QWidget*>(this->parent());
            if (!w) w = this;
            settings.setValue(QString(name()) + "/geometry", w->saveGeometry());
        }
        
	void restoreState(const QSettings& settings)
        {
            QWidget* w = static_cast<QWidget*>(this->parent());
            if (!w) w = this;
            if (settings.contains(QString(name()) + "/geometry")) {
            	w->restoreGeometry(settings.value(QString(name()) + "/geometry").toByteArray());
            }
        }
        
      protected:
        PluginWidget(const std::string& name, ohp::PluginType type)
          : ohp::PluginBase(name, type, this)
        { 
            this->setWindowTitle(QString::fromStdString(name));
            this->setAttribute(Qt::WA_DeleteOnClose, false);
            this->setVisible(false);
        }
                
        void closeEvent(QCloseEvent* e) {
	    emit windowClosed();
            QWidget::closeEvent(e);
        }
	
	void setVisible(bool visible) {
	    QApplication::setOverrideCursor(Qt::WaitCursor);
	    QWidget::setVisible(visible);
	    if (this->isVisible()) {
                subscribe(m_histograms);
                updateContent();
                windowActivated();
	    }
	    else {
		subscribe();
	    }
	    QApplication::restoreOverrideCursor();
	}

	void updateContent() {
	    QApplication::setOverrideCursor(Qt::WaitCursor);
            for(unsigned int i = 0; i < m_histograms.size(); i++) {
		histogramUpdated(m_histograms[i].c_str());
	    }
	    QApplication::restoreOverrideCursor();
	}
                
        void histogramSourceChanged()
        {
	    if (this->isVisible()) {
	        updateContent();
	    }
        }
     
      protected:
	void customEvent(QEvent *);

      private:
        HistogramsList	m_histograms;
    };    
}

#endif
