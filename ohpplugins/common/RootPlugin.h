/*
 * RootPlugin.h
 *
 *  Created on: Jan 16, 2009
 *      Author: tomasz.bold@cern.ch, andrea.dotti@cern.ch
 */

#ifndef OHPPLUGINS_ROOTPLUGIN_H_
#define OHPPLUGINS_ROOTPLUGIN_H_

#include <string>
#include <TVirtualPad.h>
#include <TObjArray.h>

struct RootPlugin
{
    virtual ~RootPlugin() {} ;

    virtual std::string doc() = 0;

    virtual bool draw(TVirtualPad* const pad, const TObjArray& histos) = 0;
};

#endif
