/*
  File:		StatusBar.h
  Author:	Serguei Kolos
*/

#ifndef OHPPLUGINS_COMMON_STATUSBAR_H
#define OHPPLUGINS_COMMON_STATUSBAR_H

#include <QLabel>
#include <QStatusBar>

namespace ohpplugins
{   
    class StatusBar
    {
      public:
	static QLabel* getMouseTrackingLabel();

	static QLabel* getObjectNameLabel();

	static QLabel* getNXLabel();

	static QLabel* getNYLabel();
        
	static QLabel* getNPagesLabel();

	static QLabel* getNHistogramsLabel();
        
	static QLabel* getMessageLabel();

	static QStatusBar* widget() { return m_widget; }

	static void setWidget(QStatusBar* widget);

      private:
	static QStatusBar* m_widget;
    };
}

#endif
