/*
 File:		TQCanvas.h
 Author:	Serguei Kolos
 */

#ifndef OHPPLUGINS_COMMON_TQCANVAS_H
#define OHPPLUGINS_COMMON_TQCANVAS_H

#include <TCanvas.h>

#include <QLabel>
#include <QSharedPointer>
#include <QWidget>

#include <ohpplugins/common/TQCanvasWatcher.h>

namespace ohpplugins {
    class TQCanvas: public QWidget {
        Q_OBJECT

    public:
        TQCanvas(QWidget* parent = 0, int width = 600, int height = 400);

        ~TQCanvas() {
            delete m_canvas;
        }

        QSize sizeHint() const;

        void setWatcher(const QString& watcher_name);

        void setWatcher(TQCanvasWatcher * watcher);

        TPad* getPad(int padid);

        void Refresh();

        void Clear();

        TCanvas* GetCanvas() {
            return m_canvas;
        }

        void cd(int pad=0) {
            m_canvas->cd(pad);
        }

    protected:
        void mouseMoveEvent(QMouseEvent*);
        void mousePressEvent(QMouseEvent*);
        void mouseReleaseEvent(QMouseEvent*);
        void changeEvent(QEvent*);
        void paintEvent(QPaintEvent*);
        void resizeEvent(QResizeEvent*);
        void showEvent(QShowEvent*);

    private:
        typedef QSharedPointer<TQCanvasWatcher> Watcher;

        TCanvas* m_canvas;
        Watcher m_watcher;
        QSize m_size;
    };
}

#endif
