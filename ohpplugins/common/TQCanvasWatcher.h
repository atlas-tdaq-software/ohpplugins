/*
  File:		TQCanvasWatcher.h
  Author:	andrea.dotti@cern.ch
  Modified:	Serguei Kolos
 */

#ifndef OHPPLUGINS_TQCANVAS_WATCHER_H
#define OHPPLUGINS_TQCANVAS_WATCHER_H

class TObject;
class TVirtualPad;

#include <QObject>
#include <QMouseEvent>

#include <ohpplugins/common/CommandDialog.h>

namespace ohpplugins
{
     /** Filters mouse events on TQCanvas.
	This class implements an event filter (see qt documentation)
	to filter mouse events directed to a TCanvas,
	The mouse button actions on the canvas have 
	a non-standard behaviour in ohp:
	- The right click context menu is disabled
	- The middle mouse button click allows to send a command
	- A left-mouse button double click opens a TCanvas with
	a (static) copy of the histograms. In this canvas the
	context menu is enabled.
	We have to disable the context menu in the TQRootCanvas since
	a new copy of the histogram can arrive to ohp while the
	user is interacting with the current version of the histogram
    */
    class TQCanvasWatcher : public QObject
    {
	Q_OBJECT
        
    public:
	TQCanvasWatcher(QObject* parent);
      
    protected:
	virtual bool eventFilter (QObject* obj, QEvent* evt);
	
        virtual bool mouseHandler(QObject* obj, QMouseEvent* evt);
      
    protected:
        TObject* findHistogram(TVirtualPad* pad) const;
        
        TVirtualPad* getPad(QObject* qobj, QMouseEvent* evt);
        
        void middleButtonPressed(QObject* qobj, QMouseEvent* evt);
        
    private:
	CommandDialog	m_command_dialog;
    };
}
#endif
