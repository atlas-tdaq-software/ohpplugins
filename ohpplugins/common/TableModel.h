/*
  File:		TableModel.h
  Author:	Serguei Kolos
*/

#ifndef OHPPLUGIN_TABLE_MODEL_H
#define OHPPLUGIN_TABLE_MODEL_H

#include <string>
#include <vector>

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/global_fun.hpp>

#include <boost/lexical_cast.hpp>

#include <QString>
#include <QVariant>
#include <QVector>
#include <QAbstractTableModel>

#include <ohp/PluginInfo.h>

using namespace boost::multi_index;

namespace ohpplugins
{
    class TableModel : public QAbstractTableModel
    {
    public:        
        typedef QVector<QVariant> RowData;
        
	struct Row
	{
	    Row(const std::string& label, int number_of_cells)
              : m_label(label.empty() ? boost::lexical_cast<std::string>(this) : label),
                m_cells(number_of_cells)
            { ; }
            
            static int s_sorting_column;
            static QString sort(const Row& row) { return row.m_cells[s_sorting_column].toString(); }
            
	    const std::string	m_label;
	    mutable RowData	m_cells;
	};
        
	typedef boost::multi_index_container<
	    Row,
	    indexed_by<
		hashed_unique <
		    BOOST_MULTI_INDEX_MEMBER(Row,const std::string,m_label)
		>,
		ordered_non_unique <
		    global_fun<const Row&,QString,&Row::sort>
		>
	    >
	> Rows;
	
        typedef Rows::nth_index<1>::type OrderedRows;
        
    public:        
        template <size_t N>
        TableModel(const QVariant (&columns)[N])
          : m_columns_number(N),
            m_columns(columns, columns + N)
        { ; }
    
        virtual void histogramUpdated(const std::string& name, bool show_masked) = 0;
        
        virtual void configure(const ohp::PluginInfo& config, std::vector<std::string>& histograms) = 0;

    public:                
        void clear();
        
        Rows::iterator addRow(const std::string& key = std::string());
        
        void removeRow(const std::string& key);
        
        Rows::iterator getRow(const std::string& key);
        
        void update(const std::string& histogram_name, bool show_masked);
        
    protected:                
	int rowCount(const QModelIndex& ) const 
        { return m_rows.size(); }
        
	int columnCount(const QModelIndex& ) const 
        { return m_columns_number; }
	
        QVariant data(const QModelIndex& index, int role) const;
	
        QVariant headerData(int section, Qt::Orientation orientation, int role) const;
        
        void sort(int column, Qt::SortOrder order = Qt::AscendingOrder);

    protected:
	typedef RowData	Columns;
        
	const int	m_columns_number;
        const Columns	m_columns;
        Rows		m_rows;
    };
}

#endif
