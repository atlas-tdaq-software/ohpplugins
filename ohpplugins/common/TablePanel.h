/*
  File:		TablePanel.h
  Author:	Serguei Kolos
*/

#ifndef OHPPLUGIN_TABLE_PANEL_H
#define OHPPLUGIN_TABLE_PANEL_H

#include <string>

#include <ohpplugins/common/PluginWidget.h>
#include <ohpplugins/common/TableModel.h>

#include <ui_TablePanel.h>

namespace ohpplugins
{
    class TablePanel :	public PluginWidget,
    			public Ui::TablePanel
    {
	Q_OBJECT
 	
    public slots:        
	virtual void on_showMaskedCheckbox_toggled(bool );
	virtual void on_copyToClipboardButton_clicked();
            
    protected:        
        TablePanel(const std::string& name);
        
        void updateContent();
                
    protected:
        bool	m_show_masked;        
    };
}

#endif
