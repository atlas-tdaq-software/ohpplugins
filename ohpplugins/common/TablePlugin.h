/*
  File:		TablePlugin.h
  Author:	Serguei Kolos
*/

#ifndef OHPPLUGIN_TABLE_PLUGIN_H
#define OHPPLUGIN_TABLE_PLUGIN_H

#include <string>

#include <ohpplugins/common/TableModel.h>
#include <ohpplugins/common/TablePanel.h>

#include <ui_TablePanel.h>

namespace ohpplugins
{
    template <class T>
    class TablePlugin : public TablePanel
    {
    public:
	TablePlugin(const std::string& name)
          : TablePanel(name)
        { tableView->setModel(&m_model); }
        
        void updateContent()
	{
	    const std::vector<std::string>& histograms = getWatchedHistograms();
            for(unsigned int i = 0; i < histograms.size(); i++) {
		m_model.update(histograms[i], m_show_masked);
	    }
	}

	void configure(const ohp::PluginInfo& config)
	{
	    std::vector<std::string> histograms;
            m_model.configure(config, histograms);
            setWatchedHistograms(histograms);
	}

	void histogramUpdated(const QString& name)
	{
	    m_model.update(name.toStdString(), m_show_masked);
	}
        
    protected:
        T	m_model;        
    };
}

#endif
