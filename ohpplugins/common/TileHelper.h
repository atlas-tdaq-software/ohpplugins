/*
 * TileHelper.h
 *
 *  Created:	Nov 4, 2008
 *  Author:	adotti
 *  Modified:	Serguei Kolos
 */

#ifndef OHPPLUGINS_TILE_HELPER_H
#define OHPPLUGINS_TILE_HELPER_H

#include <string>
#include <map>
#include <vector>

namespace ohpplugins 
{
    class TileHelper
    {
    public:	
        TileHelper();
	
	bool checkPMT(const unsigned int& pmt, const std::string& modulename);
	
        bool checkDMU(const unsigned int& dmu, const std::string& modulename);
	
        bool checkCH(const std::string& chname, const std::string& modulename);
        
	bool checkCH(unsigned int ch, const std::string& modulename);

    private:
	/** Gain enumerator */
	enum Gain {
	    LG = 0,
	    HG = 1
	};

    private:
	static void initTable(	const std::vector<std::string>& histograms,
			 	std::multimap<std::string,std::string>& dict);
                        
       /** Checks if a channel is masked
	* @returns: true if the channel is masked in DB
	* @param ch: the channel number (0-47)
	* @param modulename: the module name (example: LBA01)
	* @param gain:
	*/
	bool checkChannel(const unsigned int& ch, const std::string& modulenames,
			  const Gain& gain=HG);

	/** Checks if a channel is masked
	 * @returns: true if the channel is masked in DB
	 * @param chname: the channel name (example: B14_ch30)
	 * @param modulename: the module name (example: LBA01)
	 */
	bool checkChannel(const std::string& ch, const std::string& modulename,
			  const Gain& gain=HG);

    private:
    	static void init(const std::vector<std::string>& histograms, 
        		 std::multimap<std::string,std::string>& dict);
        
    private:
	std::multimap<std::string,std::string> m_masked_HG;	//< Masked channels (HG) map: modulename<->chname
	std::multimap<std::string,std::string> m_masked_LG;	//< Masked channels (LG) map: modulename<->chname
    };
}

#endif
