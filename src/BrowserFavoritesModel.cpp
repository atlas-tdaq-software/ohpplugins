/*
 File:		BrowserFavoritesModel.cpp
 Author:	Serguei Kolos
 */
#include <iostream>

#include <ohpplugins/BrowserFavoritesModel.h>

namespace
{
    std::ostream&
    operator<<(std::ostream& out, const QString& str)
    {
	return out << qPrintable(str);
    }
}

void 
ohpplugins::BrowserFavoritesModel::addItem(const QSharedPointer<BrowserTreeModel::Item>& item) 
{ 
    if (!item)
    	return;
        
    beginResetModel();
    m_root.addChild(item);
    endResetModel();
}

void 
ohpplugins::BrowserFavoritesModel::removeItems(const QModelIndexList& indexes)
{
    if (!indexes.size())
    	return;
       
    BrowserTreeModel::Item* item = static_cast<BrowserTreeModel::Item*>(
        		indexes[0].internalPointer());
        
    beginResetModel();
    m_root.removeChild(item);
    endResetModel();
}

QModelIndex 
ohpplugins::BrowserFavoritesModel::parent(const QModelIndex& index) const
{
    if (!index.isValid())
	return QModelIndex();

    BrowserTreeModel::Item* childItem = static_cast<Item*>(index.internalPointer());
    const BrowserTreeModel::Item* parentItem = childItem->stepparent();

    if (!parentItem)
    	parentItem = childItem->parent();

    if (parentItem == &m_root)
	return QModelIndex();

    return createIndex(parentItem->adapteeRow(), 0, (void*)parentItem);
}
