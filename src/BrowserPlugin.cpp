/*
  File:		BrowserPlugin.cpp
  Author:	Roberto Agostino Vitillo [vitillo@cli.di.unipi.it]
  Modified:	Serguei Kolos
*/
#include <algorithm>
#include <iterator>

#include <TCanvas.h>
#include <TList.h>

#include <QMessageBox>
#include <QProgressDialog>

#include <ohp/CoreInterface.h>
#include <ohp/HistogramPainter.h>

#include <ohpplugins/BrowserPlugin.h>

namespace
{
    const char * const NAME = "BrowserPlugin";
    OHP_REGISTER_PLUGIN(ohpplugins::BrowserPlugin, NAME)
}

ohpplugins::BrowserPlugin::BrowserPlugin(const std::string& name)
  : PluginWidget(name, ohp::BROWSER),
    m_tree_menu(this),
    m_favorites_menu(this),
    m_base_menu(this),
    m_use_config(true),
    m_content_up_to_date(false)
{
    setupUi(this);
    
    QList<int> s;
    s.append(300);
    s.append(100);
    splitter->setSizes(s);
    
    m_base_menu.addAction(QIcon(":/images/refresh.png"), "Refresh", this, SLOT(refreshRequested()));
    
    m_tree_menu.setDefaultAction(
    	m_tree_menu.addAction(QIcon(":/images/palette.png"), 
        	"Draw with options", this, SLOT(drawHistogramsWithConfig())));
    m_tree_menu.addAction(QIcon(":/images/pencil.png"), "Draw", this, SLOT(drawHistograms()));
    m_tree_menu.addSeparator();
    m_tree_menu.addAction(QIcon(":/images/star.png"), "Add to favorites", this, SLOT(addToFavorites()));
    m_tree_menu.addSeparator();
    m_tree_menu.addAction(QIcon(":/images/refresh.png"), "Refresh", this, SLOT(refreshRequested()));
    
    treeView->setModel(&m_tree_model);        
    treeView->viewport()->installEventFilter(this);
    treeView->header()->resizeSection(0, 300);
    treeView->header()->resizeSection(1, 100);
    treeView->setExpandsOnDoubleClick(false);
    treeView->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(treeView, SIGNAL(customContextMenuRequested(const QPoint&)), 
			SLOT(showContextMenu(const QPoint&)));
                
    m_favorites_menu.setDefaultAction(
    	m_favorites_menu.addAction(QIcon(":/images/palette.png"), 
        	"Draw with options", this, SLOT(drawHistogramsWithConfig())));
    m_favorites_menu.addAction(QIcon(":/images/pencil.png"), "Draw", this, SLOT(drawHistograms()));
    m_favorites_menu.addSeparator();
    m_favorites_menu.addAction(QIcon(":/images/remove.png"), "Remove from favorites", this, SLOT(removeFromFavorites()));
        
    favoritesView->setModel(&m_favorites_model);
    favoritesView->viewport()->installEventFilter(this);
    favoritesView->header()->resizeSection(0, 300);
    favoritesView->header()->resizeSection(1, 100);
    favoritesView->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(favoritesView, SIGNAL(customContextMenuRequested(const QPoint&)), 
			SLOT(showContextMenu(const QPoint&)));
                        
    setStyleSheet("QProgressBar { qproperty-alignment: 0x88; }");
    
    updateGeometry();
}

void 
ohpplugins::BrowserPlugin::configure(const ohp::PluginInfo& config)
{
    std::string cw;
    config.getParameter("canvasWatcher", cw);
    theCanvas->setWatcher(QString::fromStdString(cw));
}

void 
ohpplugins::BrowserPlugin::histogramUpdated(const QString& histogram_name)
{
    const std::vector<std::string>& histograms = getWatchedHistograms();
    std::vector<std::string>::const_iterator it = 
    	std::find(histograms.begin(), histograms.end(), histogram_name.toStdString());
    
    if (it != histograms.end()) {
    	int padid = std::distance(histograms.begin(),it) + 1;
        TPad* pad = theCanvas->getPad(padid);
	if (pad) {
	    if (m_use_config)
		ohp::histogram::drawWithConfig(getName(), *it, pad);
	    else
		ohp::histogram::draw(*it, pad);
	    theCanvas->Refresh();
        }
    }    
}

void 
ohpplugins::BrowserPlugin::drawHistograms(bool use_config)
{    
    m_use_config = use_config;
    
    QTreeView* v = (QTreeView*)browserTabs->currentWidget();
    std::vector<std::string> histograms = 
    	((BrowserTreeModel*)v->model())->getSelectedHistograms(v->selectionModel()->selectedIndexes());
    
    size_t histograms_num = histograms.size();
    
    if (!histograms_num) {
    	return ;
    }
    
    if (histograms_num > 50) {
	if (	QMessageBox::warning(this, 
			"Warning!",
			QString("You are requesting to paint %1 histograms, "
			"which may take a while. Do you want to proceed?").arg(histograms_num),
			QMessageBox::Yes|QMessageBox::No) != QMessageBox::Yes)
        {
            return ;
        }
    }
    
    size_t nx = ::sqrt(histograms_num);
    size_t ny = histograms_num / nx;
    if (nx*ny < histograms_num) {
	++nx;
    }
    
    {
	QProgressDialog progress("Please be patient ... or press cancel to stop", "Cancel", 0, histograms_num + 2, this);
	progress.setWindowTitle("Painting histograms");
	progress.setWindowModality(Qt::WindowModal);
	progress.setValue(1);

	theCanvas->Clear();
	theCanvas->GetCanvas()->Divide(nx, ny);

	size_t n = 0;
        for ( ; n < histograms_num; ++n) {
	    TPad* pad = theCanvas->getPad(n+1);
	    pad->Clear();
	    if (m_use_config)
		ohp::histogram::drawWithConfig(getName(), histograms[n], pad);
	    else
		ohp::histogram::draw(histograms[n], pad);

	    progress.setValue(n+2);
            
	    if (progress.wasCanceled())
		break;
	}
	
        std::vector<std::string> wh(n);
        std::copy(histograms.begin(), histograms.begin()+n, wh.begin());
        setWatchedHistograms(wh);
        
	progress.setValue(histograms_num+2);
    }
    
    theCanvas->Refresh();    
}

void 
ohpplugins::BrowserPlugin::infrastructureUp()
{
    m_tree_model.reset();
}

void 
ohpplugins::BrowserPlugin::serverUp(const QString& server)
{
    m_tree_model.serverUp(server);
}

void 
ohpplugins::BrowserPlugin::drawHistograms()
{
    drawHistograms(false);
}

void 
ohpplugins::BrowserPlugin::drawHistogramsWithConfig()
{
    drawHistograms(true);
}

void 
ohpplugins::BrowserPlugin::showContextMenu(const QPoint& )
{
    QTreeView* v = (QTreeView*)browserTabs->currentWidget();
    QModelIndexList selection = v->selectionModel()->selectedIndexes();
    
    if (selection.empty() && !browserTabs->currentIndex()) {
        m_base_menu.exec(QCursor::pos());
    	return ;
    }
    
    BrowserTreeModel::Item* item = static_cast<BrowserTreeModel::Item*>(
    					selection[0].internalPointer());
    
    if (browserTabs->currentIndex()) {
	QList<QAction *> actions = m_favorites_menu.actions();
	actions[3]->setEnabled(item->isBookmarked()); 		// Enable/Disable "Remove from favorites"
        m_favorites_menu.exec(QCursor::pos());
    }
    else {
	QList<QAction *> actions = m_tree_menu.actions();
        actions[3]->setDisabled(item->inBookmarkedBranch()); 	// Enable/Disable "Add to favorites"
	m_tree_menu.exec(QCursor::pos());
    }
}

void 
ohpplugins::BrowserPlugin::updateContent()
{
    if ( m_content_up_to_date )
    	return ;
        
    QTreeView* v = (QTreeView*)browserTabs->currentWidget();
    ((BrowserTreeModel*)v->model())->refresh(v);
    theCanvas->Clear();
    theCanvas->Refresh();
    m_content_up_to_date = true;
}

void 
ohpplugins::BrowserPlugin::refreshRequested()
{
    m_content_up_to_date = false;
    if (this->isVisible()) {
	updateContent();
    }
}

void 
ohpplugins::BrowserPlugin::histogramSourceChanged()
{
    QTreeView* v = (QTreeView*)browserTabs->currentWidget();
    v->clearSelection();
    
    refreshRequested();
}

void 
ohpplugins::BrowserPlugin::addToFavorites()
{
    QSharedPointer<ohpplugins::BrowserTreeModel::Item> item = 
    		m_tree_model.getItem(treeView->selectionModel()->selectedIndexes());
    if (item) {
	m_favorites_model.addItem(item);
    }
}

void 
ohpplugins::BrowserPlugin::removeFromFavorites()
{
    m_favorites_model.removeItems(favoritesView->selectionModel()->selectedIndexes());
}

bool 
ohpplugins::BrowserPlugin::eventFilter(QObject* obj, QEvent* evt)
{
    if (evt->type() == QEvent::MouseButtonDblClick) {
	if (browserTabs->currentIndex()) {
            m_tree_menu.defaultAction()->trigger();
        }
        else {
            m_favorites_menu.defaultAction()->trigger();
        }
	return true;
    }
    else if (evt->type() == QEvent::MouseButtonPress) {
	QTreeView* tv = (QTreeView*)browserTabs->currentWidget();
        QModelIndex i = tv->indexAt(((QMouseEvent*)evt)->pos());
        if (not i.isValid()) {
	    tv->clearSelection();
	    return true;
        }
    }
    
    return QObject::eventFilter(obj, evt);
}
