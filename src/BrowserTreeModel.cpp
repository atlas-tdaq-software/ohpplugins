/*
 File:		BrowserTreeModel.cpp
 Author:	Serguei Kolos
 */

#include <QApplication>
#include <QDebug>
#include <QTreeView>
 
#include <ohp/CoreInterface.h>

#include <ohpplugins/BrowserTreeModel.h>

QIcon* ohpplugins::BrowserTreeModel::s_node_icon;
QIcon* ohpplugins::BrowserTreeModel::s_node_star_icon;
QIcon* ohpplugins::BrowserTreeModel::s_leaf_icon;
QIcon* ohpplugins::BrowserTreeModel::s_leaf_star_icon;

namespace
{
    std::ostream&
    operator<<(std::ostream& out, const QString& str)
    {
	return out << qPrintable(str);
    }
}

ohpplugins::BrowserTreeModel::Item::Item(
    Item* parent,
    const QString& name,
    const QString& server,
    const QString& provider,
    const QString& path,
    const QIcon* icon,
    unsigned int histograms_count)
  : m_parent(parent),
    m_stepparent(0),
    m_name(name),
    m_server(server),
    m_provider(provider),
    m_path(path.length() && !path.startsWith("/") ? "/" + path : path),
    m_histograms_count(histograms_count),
    m_icon(icon)
{
    ERS_DEBUG(3, "TreeItem name='"<<m_name<<"' server='"<<m_server
    	<<"' provider='"<<m_provider<<"' path='"<<m_path<<"'");
}

bool ohpplugins::BrowserTreeModel::Item::isBookmarked() const 
{ 
    return (	m_icon == BrowserTreeModel::s_node_star_icon
	    ||	m_icon == BrowserTreeModel::s_leaf_star_icon);
}

bool ohpplugins::BrowserTreeModel::Item::inBookmarkedBranch() const 
{ 
    bool result = isBookmarked();
    
    if (!result && m_parent) {
	result = m_parent->inBookmarkedBranch();
    }
    return result;
}

void 
ohpplugins::BrowserTreeModel::Item::getSelectedHistograms(
	std::vector<std::string>& histograms) const
{
    if (m_children.empty()) {
    	histograms.push_back(getHistogramName().toStdString());
    }
    else {
    	Children::const_iterator it = m_children.begin();
        for ( ; it != m_children.end(); ++it) {
            it.value()->getSelectedHistograms(histograms);
        }
    }
}

QSharedPointer<ohpplugins::BrowserTreeModel::Item> 
ohpplugins::BrowserTreeModel::Item::getChild(Item* item) const
{
    Children::const_iterator it = m_children.begin();
    for ( ; it != m_children.end(); ++it) {
    	if (it.value() == item) {
            return it.value();
        }
    }
    return QSharedPointer<Item>();
}

void 
ohpplugins::BrowserTreeModel::Item::addChild(const QSharedPointer<Item>& child)
{
    Children::iterator it = m_children.find(child->getHistogramName());
    if (it == m_children.end()) {
        child->m_stepparent = this;
        child->m_icon = child->childCount() ? s_node_star_icon : s_leaf_star_icon;
    	m_children.insert(child->getHistogramName(), child);
    }
}

void 
ohpplugins::BrowserTreeModel::Item::removeChild(const Item* child)
{
    Children::iterator it = m_children.find(child->getHistogramName());
    if (it != m_children.end()) {
        it.value()->m_stepparent = 0;
        it.value()->m_icon = it.value()->childCount() ? s_node_icon : s_leaf_icon;
    	m_children.erase(it);
    }
}

void 
ohpplugins::BrowserTreeModel::Item::clear() 
{ 
    m_children.clear();
    updateHistogramsCount(m_histograms_count);
}

void 
ohpplugins::BrowserTreeModel::Item::updateHistogramsCount(unsigned int count)
{ 
    m_histograms_count -= count;
    
    if (m_parent)
    	m_parent->updateHistogramsCount(count);
}

int 
ohpplugins::BrowserTreeModel::Item::row() const
{ 
    if (!m_parent)
    	return 0;
        
    Children::iterator it = m_parent->m_children.find(m_name);
    int row = 0;
    for ( ; it != m_parent->m_children.begin(); --it) {
    	++row;
    }
    return row;
}

int 
ohpplugins::BrowserTreeModel::Item::adapteeRow() const
{ 
    if (!m_stepparent && !m_parent)
    	return 0;
    
    Item* parent = m_stepparent ? m_stepparent : m_parent;
        
    Children::iterator it = parent->m_children.find(m_stepparent ? getHistogramName() : m_name);
    int row = 0;
    for ( ; it != parent->m_children.begin(); --it) {
    	++row;
    }
    return row;
}

void
ohpplugins::BrowserTreeModel::Item::addChild(
	const QString& server, const QString& provider, const QString& name)
{
    if (server.isEmpty()) {
    	return;
    }
    
    ++m_histograms_count;
    Children::iterator it = m_children.find(server);
    if (it == m_children.end()) {
	Item* item = new Item(this, server, server, ".*", "", s_node_icon);
	it = m_children.insert(server, QSharedPointer<Item>(item));
    }
    it.value()->addChild(provider, name);
}

void
ohpplugins::BrowserTreeModel::Item::addChild(const QString& provider, const QString& name)
{
    if (provider.isEmpty()) {
    	return;
    }
    
    ++m_histograms_count;
    Children::iterator it = m_children.find(provider);
    if (it == m_children.end()) {
	Item* item = new Item(this, provider, m_server, provider, "", s_node_icon);
	it = m_children.insert(provider, QSharedPointer<Item>(item));
    }
    it.value()->addChild(name);
}

void
ohpplugins::BrowserTreeModel::Item::addChild(const QString& name)
{
    if (name.isEmpty()) {
    	return;
    }
    
    ++m_histograms_count;
    int i = name.indexOf('/', 1);
    if (i > 0) {
	QString first = name.left(i);
	QString rest(name.right(name.size() - i));
	Children::iterator it = m_children.find(first);
	if (it == m_children.end()) {
	    Item* item = new Item(this, first, m_server, m_provider, m_path + first, s_node_icon);
	    it = m_children.insert(first, QSharedPointer<Item>(item));
	}
	it.value()->addChild(rest);
    }
    else {
	QString id = name.startsWith('/') ? name.right(name.size()-1) : name;
        Item* item = new Item(this, id, m_server, m_provider, m_path + name, s_leaf_icon, 1);
	m_children.insert(id, QSharedPointer<Item>(item));
    }
}

/////////////////////////////////////////////////////////////////////////////////////////
//
//  BrowserTreeModel class
//
/////////////////////////////////////////////////////////////////////////////////////////
ohpplugins::BrowserTreeModel::BrowserTreeModel(QObject *parent)
  : QAbstractItemModel(parent),
    m_root(0, "root", "", ".*", ".*", s_node_icon)
{ 
    s_node_icon = new QIcon(":/images/folder.png");
    s_node_star_icon = new QIcon(":/images/folder_star.png");
    s_leaf_icon = new QIcon(":/images/histogram.png");
    s_leaf_star_icon = new QIcon(":/images/histogram_star.png");    
}

QSharedPointer<ohpplugins::BrowserTreeModel::Item> 
ohpplugins::BrowserTreeModel::getItem(const QModelIndexList& indexes) const
{
    QSharedPointer<ohpplugins::BrowserTreeModel::Item> r;
    
    if (indexes.size()) {
	Item* item = static_cast<Item*>(indexes[0].internalPointer());
	r = item->parent()->getChild(item);
    }
    return r;
}

std::vector<std::string>
ohpplugins::BrowserTreeModel::getSelectedHistograms(const QModelIndexList& indexes)
{
    std::vector<std::string> r;
    if (indexes.size()) {
    	static_cast<Item*>(indexes[0].internalPointer())->getSelectedHistograms(r);
    }
    return r;
}

int
ohpplugins::BrowserTreeModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
	return static_cast<Item*>(parent.internalPointer())->columnCount();
    else
	return m_root.columnCount();
}

QVariant
ohpplugins::BrowserTreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
	return QVariant();

    Item* item = static_cast<Item*>(index.internalPointer());

    if (role == Qt::DecorationRole && index.column() == 0) {
        return item->icon();
    }
    
    if (role != Qt::DisplayRole)
	return QVariant();

    return index.column() ? QVariant(item->histogramCount()) : QVariant(item->name());
}

Qt::ItemFlags
ohpplugins::BrowserTreeModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
	return Qt::ItemFlags();

    return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

QVariant
ohpplugins::BrowserTreeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
	return section ? "# of histograms" : "name";

    return QVariant();
}

QModelIndex
ohpplugins::BrowserTreeModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!hasIndex(row, column, parent))
	return QModelIndex();

    const Item* parentItem;

    if (!parent.isValid())
	parentItem = &m_root;
    else
	parentItem = static_cast<const Item*>(parent.internalPointer());

    Item* childItem = parentItem->child(row);
    if (childItem)
	return createIndex(row, column, childItem);
    else
	return QModelIndex();
}

QModelIndex
ohpplugins::BrowserTreeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
	return QModelIndex();

    Item* childItem = static_cast<Item*>(index.internalPointer());
    const Item* parentItem = childItem->parent();

    if (parentItem == &m_root)
	return QModelIndex();

    return createIndex(parentItem->row(), 0, (void*)parentItem);
}

int
ohpplugins::BrowserTreeModel::rowCount(const QModelIndex &parent) const
{
    if (parent.column() > 0)
	return 0;

    const Item* parentItem;
    if (!parent.isValid())
	parentItem = &m_root;
    else
	parentItem = static_cast<const Item*>(parent.internalPointer());

    return parentItem->childCount();
}

void
ohpplugins::BrowserTreeModel::reset()
{
    ERS_DEBUG(0, "Re setting the browser tree");
    
    refresh(0);
}

void 
ohpplugins::BrowserTreeModel::serverUp(const QString& server)
{
    if (m_root.hasChild(server)) {
    	return ;
    }
    beginResetModel();
    addServer(server, ".*", ".*");
    endResetModel();
}

void 
ohpplugins::BrowserTreeModel::addServer(
	const QString& server, const QString& provider, const QString& histogram)
{
    ERS_DEBUG(0, "Exploring the '"<<server<<"' server");
    std::vector<ohp::HistogramNameTokens> h;
    ohp::CoreInterface::getInstance().getHistogramsList(
	    server.toStdString(), provider.toStdString(), histogram.toStdString(), h);
    
    if (h.empty()) {
    	return ;
    }
    
    m_root.addChild(server, "", "");
    for (   std::vector<ohp::HistogramNameTokens>::iterator it = h.begin();
	    it != h.end(); ++it)
    {
	m_root.addChild(server, it->providerName.c_str(), it->histogramName.c_str());
    }
}

void
ohpplugins::BrowserTreeModel::refresh(QTreeView* tv)
{
    QModelIndexList indexes(tv ? tv->selectionModel()->selectedIndexes() : QModelIndexList());
    
    QApplication::setOverrideCursor(Qt::WaitCursor);

    if (indexes.size()) {
	QModelIndexList pindexes = persistentIndexList();
    	
	beginResetModel();
        
        Item* item = static_cast<Item*>(indexes.first().internalPointer());

	item->clear();

	addServer(item->getServer(), item->getProvider(), item->getPathPattern());        
	
        endResetModel();

	for (int i = 0; i < pindexes.size(); ++i) {
            tv->setExpanded(pindexes[i], true);
	}
        tv->selectionModel()->select(indexes.first(), QItemSelectionModel::Select);
    }
    else {
        std::vector<std::string> servers = ohp::CoreInterface::getInstance().getServers();
        
	beginResetModel();
        
        m_root.clear();
        
        for (size_t i = 0; i < servers.size(); ++i) {
	    addServer(servers[i].c_str(), ".*", ".*");
        }
        
        endResetModel();
    }

    QApplication::restoreOverrideCursor();
}
