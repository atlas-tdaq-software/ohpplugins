/*
  File:		DeathCellsPlugin.cpp
  Author:	Roberto Agostino Vitillo [vitillo@cern.ch]
  Modified:	Serguei Kolos
*/

#include <ohpplugins/DeathCellsPlugin.h>

#include <ohpplugins/common/TablePlugin.h>

#include <ohp/CoreInterface.h>

#include <TH2.h>

namespace
{
    const char * const NAME = "DeathCellsPlugin";
    OHP_REGISTER_PLUGIN(ohpplugins::TablePlugin<ohpplugins::DeathCellsPlugin>, NAME)

    QVariant headers[] = { "Module", "Channel" };
}

ohpplugins::DeathCellsPlugin::DeathCellsPlugin()
  : TableModel(headers)
{ ; }

void 
ohpplugins::DeathCellsPlugin::configure(const ohp::PluginInfo& config, std::vector<std::string>& histograms)
{
    config.getParameters("histos", histograms);
}

void 
ohpplugins::DeathCellsPlugin::histogramUpdated(const std::string& name, bool show_masked)
{
    ohp::CoreInterface& ci = ohp::CoreInterface::getInstance();
    
    TH2* histogram = static_cast<TH2*>(ci.getHistogram(name));
    if (!histogram)
	return;

    clear(); // clear the table
    
    for(int y = 1; y <= histogram->GetYaxis()->GetNbins(); y++) {
	std::string curChannel = histogram->GetYaxis()->GetBinLabel(y);
	if(curChannel.find('_') == std::string::npos) {
            continue;
        }
	for(int x = 1; x <= histogram->GetXaxis()->GetNbins(); x++) {
	    //Is it a death cell?
	    if(histogram->GetBinContent(x,y) == 0) {
		std::string curModule = histogram->GetXaxis()->GetBinLabel(x);
		if(show_masked || !m_tile_helper.checkCH(curChannel, curModule)) {
		    Rows::iterator row = addRow();
		    row->m_cells[MODULE] = QString::fromStdString(curModule);
		    row->m_cells[CHANNEL] = QString::fromStdString(curChannel);
		}
	    }
	}
    }

    sort(MODULE, Qt::AscendingOrder);
    
    delete histogram;
}
