/*
  File:		ErrorsPlugin.cpp
  Author:	Roberto Agostino Vitillo [vitillo@cern.ch]
  Modified:	Serguei Kolos
*/
#include <ohpplugins/ErrorsPlugin.h>

#include <ohpplugins/common/TablePlugin.h>

#include <ohp/CoreInterface.h>

#include <TH2.h>

namespace
{
    const char * const NAME = "ErrorsPlugin";
    const char * const LEGACY_NAME = "ErrorPlugin";
    OHP_REGISTER_PLUGIN(ohpplugins::TablePlugin<ohpplugins::ErrorsPlugin>, NAME)
    OHP_REGISTER_PLUGIN(ohpplugins::TablePlugin<ohpplugins::ErrorsPlugin>, LEGACY_NAME)

    QVariant headers[] = {  
    	"Module", "DMU", "BCID Err", "DMU Err", "NumEvents", 
	"HeadParity", "HeaderFormat", "DoubleStrobe", "SingleStrobe", "MemoryParityError"
    };
}

ohpplugins::ErrorsPlugin::ErrorsPlugin()
  : ohpplugins::TableModel(headers)
{ ; }

void
ohpplugins::ErrorsPlugin::configure(const ohp::PluginInfo& config, std::vector<std::string>& histograms)
{    
    config.getParameters("bcid", histograms);

    std::vector<std::string> dmus;
    config.getParameters("dmu", dmus);

    for(size_t i = 0; i < dmus.size(); i++) {
	histograms.push_back(dmus[i]);
    }
}

void 
ohpplugins::ErrorsPlugin::histogramUpdated(const std::string& name, bool show_masked)
{
    ohp::CoreInterface& ci = ohp::CoreInterface::getInstance();
    
    TH2* err_histogram = static_cast<TH2*>(ci.getHistogram(name));
    if (!err_histogram)
	return;
    
    for(int x = 1; x <= err_histogram->GetXaxis()->GetNbins(); x++) {
	for(int y = 1; y <= err_histogram->GetYaxis()->GetNbins(); y++) {	    
	    int error = err_histogram->GetBinContent(x,y);
	    
	    if(!error) {
            	continue;
            }
            
	    std::string x_label = err_histogram->GetXaxis()->GetBinLabel(x);
	    int dmu = err_histogram->GetYaxis()->GetBinCenter(y);
            
	    if(!show_masked && m_tile_helper.checkDMU(dmu, x_label)) {
            	continue;
            }
            
	    std::string prefix = name;
	    prefix.erase(prefix.rfind('/'));
	    prefix.erase(prefix.rfind('/'));
	    prefix.erase(prefix.rfind('/'));

	    double events = 0;
            std::string events_name = prefix + "/EXPERT/DigTests/NumSampledEvents";
	    TH2* h = static_cast<TH2*>(ci.getHistogram(events_name));
	    if (h) {
		events = h->GetEntries();
		delete h;
	    }

	    bool isDMU = false;
	    double hp = 0, hf = 0, ds = 0, ss = 0, mp = 0;
	    if (QString::fromStdString(name).endsWith("DMUError")) {
		isDMU = true;

		std::string dmu_name = prefix + "/EXPERT/DigTests/DMUError-" + x_label;
		TH2* h = static_cast<TH2*>(ci.getHistogram(dmu_name));

		if (h) {
		    hp = h->GetBinContent(1,y);
		    hf = h->GetBinContent(2,y);
		    ds = h->GetBinContent(3,y);
		    ss = h->GetBinContent(4,y);
		    mp = h->GetBinContent(5,y);
                    
		    delete h;
		}
	    }

	    std::string key = x_label + QString::number(dmu).toStdString();
	    Rows::iterator row = getRow(key);

	    if (isDMU) {
		row->m_cells[DMUERR] = QString::number(error);
		row->m_cells[HEADPARITY] = QString::number(hp);
		row->m_cells[HEADERFORMAT] = QString::number(hf);
		row->m_cells[DOUBLESTROBE] = QString::number(ds);
		row->m_cells[SINGLESTROBE] = QString::number(ss);
		row->m_cells[MEMORYPARITYERROR] = QString::number(mp);
	    }
	    else {
		row->m_cells[BCIDERR] = QString::number(error);
	    }

	    row->m_cells[MODULE] = QString::fromStdString(x_label);
	    row->m_cells[DMU] = QString::number(dmu);
	    row->m_cells[NUMEVENTS] = QString::number(events);
	}
    }
    
    delete err_histogram;
}
