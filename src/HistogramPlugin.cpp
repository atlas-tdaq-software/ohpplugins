/*
 File:		HistogramPlugin.cpp
 Author:	andrea.dotti@cern.ch
 Modified:	Serguei Kolos
 */
#include <algorithm>

#include <TCanvas.h>
#include <TList.h>

#include <ohp/HistogramPainter.h>

#include <ohp/CoreInterface.h>
#include <ohp/PluginBase.h>

#include <ohpplugins/HistogramPlugin.h>

namespace
{
    const char * const NAME = "HistogramPlugin";
    const char * const LEGACY_NAME = "HistoWindowPlugin";
    OHP_REGISTER_PLUGIN(ohpplugins::HistogramPlugin, NAME)
    OHP_REGISTER_PLUGIN(ohpplugins::HistogramPlugin, LEGACY_NAME)
}

ohpplugins::HistogramPlugin::HistogramPlugin(const std::string& name)
  : PluginWidget(name, ohp::CANVAS),
    m_nx(0),
    m_ny(0)
{
    setupUi(this);        
}

void 
ohpplugins::HistogramPlugin::configure(const ohp::PluginInfo& config)
{
    config.getParameter("ndivx", m_nx);
    config.getParameter("ndivy", m_ny);

    std::vector<std::vector<std::string> >	histograms(1);
    
    if (!config.getParameters("histos", histograms[0])) {
	histograms.clear();
        config.getParameters("layer", histograms);
    }

    size_t max_histograms_num = 0;
    std::vector<std::string> histogram_names;
    for (size_t i = 0; i < histograms.size(); ++i) {
    	max_histograms_num = std::max(max_histograms_num,histograms[i].size());
        
        for(size_t j = 0; j < histograms[i].size(); ++j) {
            m_histograms.insert(Histogram(histograms[i][j],j+1));
            
            if (std::find(histogram_names.begin(),
            		histogram_names.end(),histograms[i][j]) == histogram_names.end())
            {
            	histogram_names.push_back(histograms[i][j]);
            }
        }
    }
    
    setWatchedHistograms(histogram_names);
    
    if (max_histograms_num > (m_nx*m_ny)) {
    	m_nx = ::sqrt(max_histograms_num);
        m_ny = max_histograms_num / m_nx;
        if (m_nx*m_ny < max_histograms_num) {
            ++m_nx;
        }
    }
    
    theCanvas->GetCanvas()->Divide(m_nx, m_ny);
    theCanvas->GetCanvas()->SetHighLightColor(10);

    std::string cw;
    config.getParameter("canvasWatcher", cw);
    theCanvas->setWatcher(QString::fromStdString(cw));
}

void 
ohpplugins::HistogramPlugin::histogramUpdated(const QString& histogram_name)
{
    drawHistogram(histogram_name.toStdString());
    theCanvas->Refresh();
}

void 
ohpplugins::HistogramPlugin::drawHistogram(const std::string& histogram_name)
{
    std::pair<Histograms::iterator,Histograms::iterator> same_name = 
    				m_histograms.get<0>().equal_range(histogram_name);
                                
    for(Histograms::iterator it = same_name.first; it != same_name.second; ++it)
    {
	std::pair<HistogramsPadOrdered::iterator,HistogramsPadOrdered::iterator> same_pad = 
    				m_histograms.get<1>().equal_range(it->m_pad_id);
                                
	TPad* pad = theCanvas->getPad(it->m_pad_id);
	pad->Clear();
        std::string options;
	for(HistogramsPadOrdered::iterator pit = same_pad.first; pit != same_pad.second; ++pit)
	{
	    ERS_DEBUG(1, "Draw '"<<pit->m_name<<"' histogram in the pad #"<<it->m_pad_id
            		<<" of the '"<<getName()<<"' tab");
            ohp::histogram::drawWithConfig(getName(), pit->m_name, pad, options);
            options = "same";
        }
    }
}

void
ohpplugins::HistogramPlugin::updateContent()
{
    if (m_histograms.empty())
    	return;

    int start = m_histograms.get<1>().begin()->m_pad_id;
    int end   = m_histograms.get<1>().rbegin()->m_pad_id;
    for (int i = start; i <= end; ++i) {
    	drawHistogram(m_histograms.get<1>().find(i)->m_name);
    }
    theCanvas->Refresh();
}

void 
ohpplugins::HistogramPlugin::resumed()
{
    if (isVisible()) {
	updateContent();
    }
}
