/*
 File:		HistogramRootPlugin.cpp
 Author:	andrea.dotti@cern.ch, tomasz.bold@cern.ch
 Modified:	Serguei Kolos
 */
#include <algorithm>

#include <ers/ers.h>

#include <ohp/CoreInterface.h>

#include <ohpplugins/HistogramRootPlugin.h>

#include <ohpplugins/common/RootPlugin.h>

#include <TCanvas.h>
#include <TPluginManager.h>

namespace
{
    const char * const NAME = "HistogramRootPlugin";
    OHP_REGISTER_PLUGIN(ohpplugins::HistogramRootPlugin, NAME)
}

ohpplugins::HistogramRootPlugin::HistogramRootPlugin(const std::string& name)
  : PluginWidget(name, ohp::CANVAS),
    m_timeout(0),
    m_require_all_histograms(false)
{ 
    setupUi(this);
}

void
ohpplugins::HistogramRootPlugin::configure(const ohp::PluginInfo& config)
{
    std::vector<std::string> histograms;
    
    config.getParameters("histos", histograms);
    setWatchedHistograms(histograms);
    
    std::string cw;
    config.getParameter("canvasWatcher", cw);
    theCanvas->setWatcher(QString::fromStdString(cw));

    config.getParameter("timeout", m_timeout);
    config.getParameter("require-all-histos", m_require_all_histograms);

    std::string classname, library, constructor;
    config.getParameter("classname", classname);
    config.getParameter("macroname", library);
    if (!config.getParameter("constructor", constructor)) {
	constructor = classname + "()";
    }
    
    if (!createRootPlugin(classname, library, constructor)) {
	ERS_LOG("Failed to create '"<<classname<<"' ROOT Plugin");
    }
}

bool 
ohpplugins::HistogramRootPlugin::createRootPlugin(
	const std::string& className, 
        const std::string& library, 
        const std::string& constructor)
{
    TPluginManager manager;
    manager.AddHandler("RootPlugin", className.c_str(), 
    	className.c_str(), library.c_str(), constructor.c_str(), "OHP");

    TPluginHandler* handler = manager.FindHandler("RootPlugin", className.c_str());
    if (!handler) {
	ERS_LOG("Cannot find handler for '"<<className<<"' ROOT plugin");
	return false;
    }

    if (handler->LoadPlugin()) {
	ERS_LOG("Can't load ROOT plugin '"<<className<<"' from the '"<<library<<"' library");
	return false;
    }
    
    m_root_plugin.reset((RootPlugin*) handler->ExecPlugin(0));
    
    m_timer.start();
    return true;
}

void 
ohpplugins::HistogramRootPlugin::histogramUpdated(const QString& )
{
    if (m_root_plugin) {
    	update();
    }
}

void 
ohpplugins::HistogramRootPlugin::updateContent() 
{
    if (m_root_plugin) {
    	update();
    }
}

void 
ohpplugins::HistogramRootPlugin::update()
{
    if (m_timer.elapsed() < m_timeout) {
    	return ;
    }

    const HistogramsList& histogram_names = getWatchedHistograms();
    
    TObjArray histograms(histogram_names.size());
    
    for (size_t i = 0; i < histogram_names.size(); ++i) {
	TObject* h = ohp::CoreInterface::getInstance().getHistogram(histogram_names[i]);
	if ( h ) histograms.Add(h);
    }
    
    if (   (m_require_all_histograms  && (size_t)histograms.GetEntries() == histogram_names.size())
    	|| (!m_require_all_histograms && histograms.GetEntries()))
    {
	TCanvas* rootCanvas = theCanvas->GetCanvas();
	rootCanvas->cd();
	try {
            if (m_root_plugin->draw(rootCanvas, histograms)) {
		theCanvas->Refresh();
	    }
	}
	catch ( ... ) {
	    m_root_plugin.reset();
	}
    }
    
    m_timer.restart();
}
