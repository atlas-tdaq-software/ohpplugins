/*
 File:		HistogramTabPlugin.cpp
 Author:	andrea.dotti@cern.ch
 MODIFIED:	Roberto Agostino Vitillo [vitillo@cli.di.unipi.it] (Mid 2008) -- MDI support added
 MODIFIED:	Serguei Kolos
 */

#include <ohpplugins/HistogramTabPlugin.h>

#include <QTabWidget>

namespace
{
    const char * const NAME = "HistogramTabPlugin";
    const char * const LEGACY_NAME = "HistoWindowTabPlugin";
    OHP_REGISTER_PLUGIN(ohpplugins::HistogramTabPlugin, NAME)
    OHP_REGISTER_PLUGIN(ohpplugins::HistogramTabPlugin, LEGACY_NAME)
}

ohpplugins::HistogramTabPlugin::HistogramTabPlugin(const std::string& name)
  : PluginWidget(name, ohp::CANVAS)
{
    setupUi(this);
}

void
ohpplugins::HistogramTabPlugin::configure(const ohp::PluginInfo& config)
{
    std::vector<std::string> tabs;
    config.getParameters("tabs", tabs);
    
    for (size_t i = 0; i < tabs.size(); ++i) {
    	try {
            ohp::PluginInfo tab_info = config.getInfo(tabs[i]);
            
            ohp::Options::iterator it = config.m_options.begin();
            for ( ; it != config.m_options.end(); ++it) {
            	if (it->m_key == "tabs")
                    continue;
                    
                ohp::Options::iterator tit = tab_info.m_options.find(it->m_key);
                if (tit == tab_info.m_options.end()) {
                    tab_info.m_options.insert(*it);
                }
            }
            
            boost::shared_ptr<EventReceiver> tab;
	    std::vector<std::string> nested_tabs;
	    tab_info.getParameters("tabs", nested_tabs);
            if (nested_tabs.empty()) {
                HistogramPlugin* p = new HistogramPlugin(tabs[i]);
                p->configure(tab_info);
		histogramTabs->addTab(p, tabs[i].c_str());
                tab.reset(p);
            }
            else {
                HistogramTabPlugin* p = new HistogramTabPlugin(tabs[i]);
                p->configure(tab_info);
		histogramTabs->addTab(p, tabs[i].c_str());
                tab.reset(p);
            }
	    
            m_tabs.push_back(tab);
        }
        catch(ohp::PluginInfoNotFound & ex) {
            ers::error(ex);
        }
    }    
}

void 
ohpplugins::HistogramTabPlugin::setVisible(bool visible)
{
    QWidget::setVisible(visible);
    if (histogramTabs->currentWidget()) {
    	histogramTabs->currentWidget()->setVisible(visible);
    }
}

void
ohpplugins::HistogramTabPlugin::updateContent()
{
    for (size_t i = 0; i < m_tabs.size(); ++i) {
	m_tabs[i]->resumed();
    }
}

void 
ohpplugins::HistogramTabPlugin::resumed()
{
    if (isVisible()) {
    	updateContent();
    }
}
