/*
  File:		HotCellsPlugin.cpp
  Author:	Roberto Agostino Vitillo [vitillo@cern.ch]
  Modified:	Serguei Kolos
*/
#include <ohpplugins/HotCellsPlugin.h>

#include <ohpplugins/common/TablePlugin.h>

#include <ohp/CoreInterface.h>

#include <TH2.h>

namespace
{
    const char * const NAME = "HotCellsPlugin";
    OHP_REGISTER_PLUGIN(ohpplugins::TablePlugin<ohpplugins::HotCellsPlugin>, NAME)

    QVariant headers[] = { "Module", "Channel", "Value" };
}

ohpplugins::HotCellsPlugin::HotCellsPlugin()
  : TableModel(headers),
    m_multiplier(0)
{ ; }

void 
ohpplugins::HotCellsPlugin::configure(const ohp::PluginInfo& config, std::vector<std::string>& histograms)
{
    config.getParameters("histos", histograms);
    config.getParameter ("sigma",  m_multiplier);
}

void 
ohpplugins::HotCellsPlugin::histogramUpdated(const std::string& name, bool show_masked)
{
    ohp::CoreInterface& ci = ohp::CoreInterface::getInstance();
    
    TH2* histogram = static_cast<TH2*>(ci.getHistogram(name));
    if (!histogram)
	return;
    
    clear();	// clear the table
    
    Double_t average = 0;
    unsigned numBins = 0;
    
    for(int y = 1; y <= histogram->GetYaxis()->GetNbins(); y++) {
	std::string curChannel = histogram->GetYaxis()->GetBinLabel(y);
	if(curChannel.find('_') == std::string::npos) {
            continue;
        }

	for(int x = 1; x <= histogram->GetXaxis()->GetNbins(); x++) {
	    if(!histogram->GetBinContent(x,y)) {
            	continue;
            }
            
	    std::string curModule = histogram->GetXaxis()->GetBinLabel(x);
	    if(show_masked || !m_tile_helper.checkCH(curChannel, curModule)){
		average += histogram->GetBinContent(x,y);
		numBins++;
	    }
	}
    }

    Double_t testValue = m_multiplier*average/(double)numBins;
    for(int y = 1; y <= histogram->GetYaxis()->GetNbins(); y++) {
	std::string curChannel = histogram->GetYaxis()->GetBinLabel(y);
	if(curChannel.find('_') == std::string::npos) {
            continue;
        }
        
	for(int x = 1; x <= histogram->GetXaxis()->GetNbins(); x++) {
	    if(histogram->GetBinContent(x,y) <= testValue) {
            	continue;
            }
            
	    std::string curModule = histogram->GetXaxis()->GetBinLabel(x);
	    if(show_masked || !m_tile_helper.checkCH(curChannel, curModule)) {
		Rows::iterator row = addRow();
		row->m_cells[MODULE]  = QString::fromStdString(curModule);
		row->m_cells[CHANNEL] = QString::fromStdString(curChannel);
		row->m_cells[VALUE]   = QString().setNum(histogram->GetBinContent(x,y));
	    }
	}
    }

    sort(MODULE, Qt::AscendingOrder);
    
    delete histogram;
}
