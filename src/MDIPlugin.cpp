/*
 File:		MDIPlugin.cpp
 Author:	Roberto Agostino Vitillo [vitillo@cli.di.unipi.it]
 Modified:	Serguei Kolos
 */

#include <boost/tokenizer.hpp>
#include <boost/date_time/c_local_time_adjustor.hpp>

#include <TDirectory.h>
#include <TFile.h>

#include <ohpplugins/MDIPlugin.h>
#include <ohpplugins/common/StatusBar.h>

#include <ohp/CoreInterface.h>
#include <ohp/Configuration.h>
#include <ohp/FileHistogramManager.h>
#include <ohp/OnlineHistogramManager.h>

#include <QDebug>
#include <QString>
#include <QPushButton>
#include <QLayout>
#include <QLabel>
#include <QApplication>
#include <QMessageBox>
#include <QEvent>
#include <QSpinBox>
#include <QDockWidget>
#include <QTabWidget>
#include <QFileDialog>
#include <QMdiSubWindow>
#include <QProgressDialog>
#include <QDialog>
 
#include <ui_AboutDialog.h>

namespace ohpplugins
{
    const char * const NAME = "MDIPlugin";
    OHP_REGISTER_PLUGIN(ohpplugins::MDIPlugin, NAME)
            
    struct TabWidget : public QWidget
    {
	void setVisible(bool state) {
	    QWidget::setVisible(state);
	    if (layout()->itemAt(0)) {
		QWidget* w = layout()->itemAt(0)->widget();
		w->setVisible(state);
	    }
	}
    };
}

ohpplugins::CheckBox::CheckBox(
    QWidget* parent,
    ohp::PluginBase* plugin,
    QLayout* window,
    QLayout* tab)
  : QPushButton(parent),
    m_plugin(plugin->getWidget()),
    m_layout(window),
    m_tab(tab),
    m_visibility(m_plugin->isVisible())
{
    setWhatsThis(QString::fromStdString(plugin->getDoc()));
    
    QIcon wicon = m_plugin->windowIcon();
    m_layout->parentWidget()->setAttribute(Qt::WA_DeleteOnClose, false);
    m_layout->parentWidget()->setWindowIcon(wicon);
    
    setCheckable(true);
    setText(plugin->getName().c_str());

    QIcon icon;
    icon.addPixmap(wicon.pixmap(16, QIcon::Normal,   QIcon::On), QIcon::Normal, QIcon::On);
    icon.addPixmap(wicon.pixmap(16, QIcon::Disabled, QIcon::On), QIcon::Normal, QIcon::Off);
    setIcon(icon);
    setStyleSheet("QPushButton { border: none; background-color: none; \
    				 text-align: left; padding: 1px; margin: 0; }");       
    connect(this, SIGNAL(toggled(bool)), SLOT(setPluginVisible(bool)));
    connect(m_plugin,SIGNAL(windowClosed()),SLOT(windowClosed()));
    
    // Without this code the MDI child windows will come up for the first time
    // in minimized state. The issue is that the MDI area widget does not set 
    // proper geometry for its children if they have been added when it was invisible.
    QWidget * w = m_layout->parentWidget();
    w->show();
    QRect r = w->geometry();
    r.setSize(w->sizeHint());
    w->setGeometry(r);
    w->hide();
}

void 
ohpplugins::SpinBox::stepBy( int steps )
{
    m_pos += steps;
    if ( m_pos < 0 )
	m_pos = 0;
    else if ( m_pos >= (int)m_values.size() )
	m_pos = (int)m_values.size() - 1;
    if ( m_pos >= 0 ) {
	std::set<daq::mda::RunInfo>::iterator it = m_values.begin();
	std::advance(it, m_pos);
	setValue(it->run());

	boost::posix_time::ptime universal_time(
        	boost::posix_time::from_time_t(it->time()));
        boost::posix_time::ptime local_time(
        	boost::date_time::c_local_adjustor<boost::posix_time::ptime>::utc_to_local(universal_time));
	std::string s(boost::posix_time::to_simple_string(local_time));
	setSuffix(QString(" EoR @ %1").arg(QString::fromStdString(s)));
    }
}

//////////////////////////////////////////////////////////////////////////////////
//
// MDIPlugin class
//
//////////////////////////////////////////////////////////////////////////////////
ohpplugins::MDIPlugin::MDIPlugin(const std::string& name)
  : ohp::PluginBase(name, ohp::MAIN, this),
    m_font_size(qApp->font().pointSize()),
    m_actions_group(this),
    m_sources_group(this),
    m_files_group(this)
{    
    setupUi(this);
    
    m_actions_group.addAction(actionStart);
    m_actions_group.addAction(actionPause);
    
    setWindowTitle("Online Histogram Presenter");
    
    while (pluginToolBox->count()) {
    	pluginToolBox->removeItem(0);
    }
    
    StatusBar::setWidget(statusBar());
    
    try {
        m_archive.reset(new ohp::ArchiveHistogramManager(100000));

        menuInputArchive->addAction(
            new RunNumberSelector(menuSystem, m_archive->getRunInfos(), this,
                    SLOT(inputRunNumberSelected(unsigned int, const QString& ))));
    } catch(std::exception & ex) {
        ERS_LOG("Cannot access histogram archive: " << ex.what());
    }
    
    menuInputFile->menuAction()->setCheckable(true);
    menuInputArchive->menuAction()->setCheckable(true);
        
    m_sources_group.addAction(actionInputOnlineSystem);
    m_sources_group.addAction(menuInputFile->menuAction());
    m_sources_group.addAction(menuInputArchive->menuAction());
        
    menuInputSource->installEventFilter(this);
}

void 
ohpplugins::MDIPlugin::inputFileToggled()
{
    QApplication::setOverrideCursor(Qt::WaitCursor);

    QAction* a = (QAction*)sender();
    
    boost::shared_ptr<ohp::HistogramManager> m(
    		new ohp::FileHistogramManager(a->text().toStdString()));
    ohp::CoreInterface::getInstance().setHistogramSource(m);
        
    menuInputFile->setTitle(QString("File [%1]").arg(a->text()));
    menuInputFile->menuAction()->setChecked(true);
    setWindowTitle(QString("Online Histogram Presenter [Offline Mode, File=%1]").arg(a->text()));

    actionPause->toggle();
    actionStart->setEnabled(false);
    actionPause->setEnabled(false);
    
    QApplication::restoreOverrideCursor();
}

void 
ohpplugins::MDIPlugin::inputRunNumberSelected(unsigned int run_number, const QString & time)
{
    QApplication::setOverrideCursor(Qt::WaitCursor);
    
    m_archive->setRunNumber(run_number);
    
    menuInputArchive->setTitle(QString("Archive [%1 %2]").arg(run_number).arg(time));
    menuInputArchive->menuAction()->setChecked(true);
    setWindowTitle(QString("Online Histogram Presenter [Archive Mode, Run Number=%1 %2]").arg(run_number).arg(time));
    
    actionPause->toggle();
    actionStart->setEnabled(false);
    actionPause->setEnabled(false);

    ohp::CoreInterface::getInstance().setHistogramSource(m_archive);
    
    QApplication::restoreOverrideCursor();
}

void 
ohpplugins::MDIPlugin::on_actionInputOnlineSystem_toggled(bool on)
{
    if ( !on )
    	return;
        
    QApplication::setOverrideCursor(Qt::WaitCursor);
    
    boost::shared_ptr<ohp::HistogramManager> m(
    		new ohp::OnlineHistogramManager(
                	ohp::CoreInterface::getInstance().getPartition()));
    ohp::CoreInterface::getInstance().setHistogramSource(m);
    setWindowTitle(QString("Online Histogram Presenter"));
        
    actionStart->setEnabled(true);
    actionPause->setEnabled(true);
    actionStart->toggle();
    
    QApplication::restoreOverrideCursor();
}

void 
ohpplugins::MDIPlugin::aboutToDisplay( ) 
{
    QSettings settings("CERN", "OHP");

    Plugins::iterator it = m_plugins.begin();
    for ( ; it != m_plugins.end(); ++it) {
        (*it)->restoreState(settings);
    }

    actionInputOnlineSystem->toggle();
    
    updateFontSize();
}

void 
ohpplugins::MDIPlugin::closeEvent(QCloseEvent* )
{
    {
	QSettings settings("CERN", "OHP");

	Plugins::iterator it = m_plugins.begin();
	for ( ; it != m_plugins.end(); ++it) {
	    (*it)->saveState(settings);
	}
    }
    // That's necessary since sometimes the application
    // hangs in the ROOT event loop due to the ROOT bug
    // http://savannah.cern.ch/bugs/?89911    
    exit(0);
}

void 
ohpplugins::MDIPlugin::configure(const ohp::PluginInfo& config)
{
    std::map<std::string,std::string> panels;
    std::vector<std::string> panels_config;
    if (config.getParameters("panels", panels_config)) {
	std::vector<std::string>::iterator it;
	for (it = panels_config.begin(); it != panels_config.end(); ++it) {
	    std::vector<std::string> views;
	    if (!config.getParameters(*it, views)) {
		continue;
	    }

	    std::vector<std::string>::iterator pit;
	    for (pit = views.begin(); pit != views.end(); ++pit) {
		panels.insert(std::make_pair(*pit, *it));
	    }
	}
    }
    
    const std::vector<ohp::PluginConfig>& plugins_configurations 
    		= ohp::CoreInterface::getInstance().getConfiguration().getPlugins();
    const ohp::Plugins& plugins 
    		= ohp::CoreInterface::getInstance().getPlugins();
    
    typedef std::map<std::string, std::pair<QVBoxLayout*,QTabWidget*> > Layouts;
    Layouts layouts;
    
    // We have to iterate over the plugins_configurations vector as it keeps the same
    // plugins order as in the configuration file.
    // Contrary, the ohp::Plugins is a map which is already sorted alphabetically
    for (std::vector<ohp::PluginConfig>::const_iterator it = plugins_configurations.begin(); 
    		it != plugins_configurations.end(); ++it) 
    {
	ohp::Plugins::const_iterator plit = plugins.find(it->getName());
        if (plit == plugins.end()) {
	    ERS_DEBUG(1, "The '" << it->getName() << "' plugin was not instantiated, skipping it.");
            continue;
        }
        
        boost::shared_ptr<ohp::PluginBase> plugin = plit->second;
	m_plugins.push_back(plugin);
        
	ERS_DEBUG(1, "Processing '" << plugin->getName() << "' plugin");
        
	QWidget* gui = plugin->getWidget();
	if (!gui) {
            continue;
        }

	if (plugin->getType() == ohp::MAIN) {
            continue;
        }

        if (plugin->getType() == ohp::STATUS) {
            QDockWidget* dock = new QDockWidget(plugin->getName().c_str(), this);
            dock->setObjectName(plugin->getName().c_str());
            gui->setParent(dock);
            dock->setWidget(gui);
            addDockWidget(Qt::RightDockWidgetArea, dock);
            
	    continue;
	}
	
	QObject::connect(mdiWorkspace,SIGNAL(subWindowActivated(QMdiSubWindow*)),
        		 gui,SLOT(subWindowActivated(QMdiSubWindow*)));
        
        mdiWorkspace->addSubWindow(gui);
        
        std::string section_name;
	std::map<std::string, std::string>::const_iterator pit;
	if((pit = panels.find(plugin->getName())) != panels.end()) {
	    section_name = pit->second;
	}
        else {
	    if (plugin->getType() == ohp::CANVAS) {
		section_name = "Histograms";
            }
	    else if (plugin->getType() == ohp::TABLE) {
		section_name = "Tables";
            }
	    else if (plugin->getType() == ohp::BROWSER) {
                section_name = "Browser";
            }
            else {
		section_name = "Others";
            }
	}

	Layouts::iterator lit = layouts.find(section_name);
        QVBoxLayout* layout;
        QTabWidget*  tab;
        if (lit == layouts.end()) {
	    QWidget* w = new QWidget();
	    layout = new QVBoxLayout(w);
            layout->setSpacing(0);
            
	    pluginToolBox->addItem(w, section_name.c_str());
            
            if (plugin->getType() == ohp::BROWSER) {
            	tab = tabWorkspace;
            }
            else {
		tab = new QTabWidget();
		tabWorkspace->addTab(tab, section_name.c_str());
	    }
	    layouts.insert(std::make_pair(section_name, std::make_pair(layout,tab)));
	} 
        else {
	    layout = lit->second.first;
	    tab = lit->second.second;
	}

	ERS_DEBUG(1, "Adding '" << plugin->getName() << "' plugin to the tool box");
        QWidget* placeholder = new TabWidget();
	QVBoxLayout* phl = new QVBoxLayout(placeholder);
        phl->setContentsMargins(0,0,0,0);
	CheckBox* button = new CheckBox(layout->parentWidget(), 
        		plugin.get(), gui->parentWidget()->layout(), phl);
                        
        m_buttons.push_back(button);
        
	layout->addWidget(button);
        
        tab->addTab(placeholder, plugin->getName().c_str());
    }
     
    Layouts::iterator lit = layouts.begin();
    for ( ; lit != layouts.end(); ++lit) {
    	lit->second.first->addStretch(1);
    }
}

void ohpplugins::MDIPlugin::saveState(QSettings& settings)
{
    QString n(name());
    settings.setValue(n + "/geometry", saveGeometry());    
    settings.setValue(n + "/windowState", QMainWindow::saveState());
    settings.setValue(n + "/fontSize", m_font_size);
    settings.setValue(n + "/viewAsWindows", actionViewAsWindows->isChecked());
}

void ohpplugins::MDIPlugin::restoreState(const QSettings& settings)
{
    QString n(name());
    restoreGeometry(settings.value(n + "/geometry").toByteArray());
    QMainWindow::restoreState(settings.value(n + "/windowState").toByteArray());
    m_font_size = settings.value(n + "/fontSize", m_font_size).toInt();
    bool viewAsWindows = settings.value(n + "/viewAsWindows", true).toBool();
    if (viewAsWindows) {
    	actionViewAsWindows->toggle();
    }
    else {
    	actionViewAsTabs->toggle();
    }
}

void 
ohpplugins::MDIPlugin::on_actionStart_toggled(bool state)
{
    if (state) {
    	actionPause->setChecked(false);
        ohp::CoreInterface::getInstance().resume();
    }
}

void 
ohpplugins::MDIPlugin::on_actionPause_toggled(bool state)
{
    if (state) {
    	actionStart->setChecked(false);
    	ohp::CoreInterface::getInstance().pause();
    }
}

void 
ohpplugins::MDIPlugin::on_actionViewAsWindows_toggled(bool state)
{
    if (state) {
    	actionViewAsTabs->setChecked(false);
        pluginDockWidget->setVisible(true);
        stackedWorkspaces->setCurrentIndex(0);
        actionTile->setEnabled(true);
        actionCascade->setEnabled(true);
        actionNext->setEnabled(true);
        actionPrevious->setEnabled(true);
        for (size_t i = 0; i < m_buttons.size(); ++i) {
	    m_buttons[i]->showInWindow();
        }
    }
}

void 
ohpplugins::MDIPlugin::on_actionViewAsTabs_toggled(bool state)
{
    if (state) {
    	actionViewAsWindows->setChecked(false);
        pluginDockWidget->setVisible(false);
        stackedWorkspaces->setCurrentIndex(1);
        actionTile->setEnabled(false);
        actionCascade->setEnabled(false);
        actionNext->setEnabled(false);
        actionPrevious->setEnabled(false);
        for (size_t i = 0; i < m_buttons.size(); ++i) {
	    m_buttons[i]->showInTab();
        }
    }
}

void 
ohpplugins::MDIPlugin::on_actionRefresh_triggered()
{
    if (actionViewAsWindows->isChecked()) {
	QMdiSubWindow* sw = mdiWorkspace->activeSubWindow();
	if (!sw)
	    return;

	Plugins::iterator it = m_plugins.begin();
	for ( ; it != m_plugins.end(); ++it) {
	    if (sw->widget() == (*it)->getWidget()) {
		ERS_DEBUG(0, "updateContent is called on '"<<(*it)->getName()<<"' plugin");
		(*it)->updateContent();
		break;
	    }
	}
    }
    else {
	Plugins::iterator it = m_plugins.begin();
	for ( ; it != m_plugins.end(); ++it) {
	    if ((*it)->getWidget()->isVisible()) {
		ERS_DEBUG(0, "updateContent is called on '"<<(*it)->getName()<<"' plugin");
                (*it)->updateContent();
	    }
	}
    }
}

void 
ohpplugins::MDIPlugin::updateFontSize()
{
    Plugins::iterator it = m_plugins.begin();
    for ( ; it != m_plugins.end(); ++it) {
	(*it)->getWidget()->setStyleSheet(QString("QWidget { font-size: %1pt }").arg(m_font_size));
    }
}

void 
ohpplugins::MDIPlugin::on_actionIncreaseFontSize_triggered()
{
    m_font_size += 2;
    updateFontSize();
}

void 
ohpplugins::MDIPlugin::on_actionDecreaseFontSize_triggered()
{
    if (m_font_size > 2) {
	m_font_size -= 2;
	updateFontSize();
    }
}

void
ohpplugins::MDIPlugin::on_actionOpen_triggered()
{
    QString f = QFileDialog::getOpenFileName(
			this,
                        "",
                        "Choose file",
    			"ROOT files (*.root)");
    if(f == "")
	return;
    
    QAction* a = menuInputFile->addAction(f, this, SLOT(inputFileToggled()));
    a->setCheckable(true);
    m_files_group.addAction(a);
    a->trigger();
}

void 
ohpplugins::MDIPlugin::on_actionAbout_triggered()
{
    Ui::AboutDialog ui;
    QDialog dialog(this);
    ui.setupUi(&dialog);
    dialog.exec();
}

void
ohpplugins::MDIPlugin::on_actionSaveAs_triggered()
{
    QString s = QFileDialog::getSaveFileName(
			this,
                        "",
                        "Choose file",
    			"ROOT files (*.root)");

    if(s == "")
	return;

    if(!s.endsWith(".root")) {
	s += ".root";
    }

    writeToFile(s.toStdString());
}

TDirectory* 
ohpplugins::MDIPlugin::createTDirectory(const std::string& path, TFile* file)
{
    typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
    boost::char_separator<char> separators("/");

    tokenizer tokens(path,separators);
    
    TDirectory* current = file;
    for (tokenizer::iterator it = tokens.begin(); it != tokens.end(); ++it) {
	if (current->Get(it->c_str())) {
	    current->cd(it->c_str());
	    current = gDirectory;
	} 
        else {
	    current = current->mkdir(it->c_str());
	}
        if (!current) {
            break;
        }
    }
    return current;
}

void
ohpplugins::MDIPlugin::writeToFile(const std::string & filename)
{    
    TFile* file = TFile::Open(filename.c_str(), "recreate");
    if (!file) {
	return;
    }
    
    ohp::Histograms histograms;
    ohp::CoreInterface::getInstance().getHistograms(histograms);

    QProgressDialog progress("Saving file...", "Abort", 0, histograms.size(), this);
    progress.setCancelButton(0);
    
    ohp::Histograms::iterator it  = histograms.begin();
    ohp::Histograms::iterator end = histograms.end();
    
    int saved = 0;
    for (; it != end; ++it) {
	QApplication::processEvents();
	progress.setValue(saved);

	if (!it->second) {
	    continue;
	}
	
        std::string::size_type pos = it->first.find_last_of('/');
	std::string path = it->first.substr(0, pos);
	std::replace(path.begin(), path.end(), ':', '_');
	std::string name = it->first.substr(pos+1);
	TDirectory* dir = createTDirectory(path, file);
	if (!dir) {
	    ERS_LOG( "Unable to create '" << path << "' directory" );
            continue;
	}
        
	dir->cd();
	it->second->Write(name.c_str());
	++saved;
    }
    file->Close();
}
