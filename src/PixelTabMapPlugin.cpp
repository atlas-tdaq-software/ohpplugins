/*
  File:		PixelTabMapPlugin.cpp
  Author:	Giulia Ucchielli
  Revision :    Antonio De Maria, Nanjing University, antonio.de.maria@cern.ch  
*/

#include <ohpplugins/PixelTabMapPlugin.h>
#include <ohpplugins/common/TablePlugin.h>

#include <ohp/CoreInterface.h>

#include <QLabel>
#include <QString>
#include <QLCDNumber>
#include <QPushButton>
#include <QLayout>
#include <QTextEdit>
#include <QGroupBox>

#include <QClipboard>
#include <QApplication>

#include <dqmf/is/Result.h>

#include <TH2.h>

#include <ohp/HistogramPainter.h>

#include <ohp/CoreInterface.h>
#include <ohp/PluginBase.h>
#include <ohp/HistogramPainter.h>

#include <ohp/Configuration.h>
#include <ohp/HistogramProperties.h>

#include <dqmf/is/Result.h>


namespace
{
    const char * const NAME = "PixelTabMapPlugin";
    OHP_REGISTER_PLUGIN(ohpplugins::TablePlugin<ohpplugins::PixelTabMapPlugin>, NAME)

    QVariant headers[] = { "ALL", "IBL", "IBLA", "IBLC", "L0", "L1", "L2", "ECA", "ECC"}; // Columns headers
}

ohpplugins::PixelTabMapPlugin::PixelTabMapPlugin()
  : TableModel(headers),
    m_threshold(0)
{ ; }

/* function currently not used. Kept for future development
std::vector<QVariant> ohpplugins::PixelTabMapPlugin::manipulateVerticalHeader(std::vector<std::string> lineArray){

    std::vector<QVariant> tmp;
    for(unsigned int l=0; l < lineArray.size(); l++){
      while(lineArray[l].find("_") != std::string::npos)
	lineArray[l].replace(lineArray[l].find("_"), 1, " ");
      tmp.push_back(QString::fromStdString(lineArray[l]));
    }
    return tmp;
}
*/

void ohpplugins::PixelTabMapPlugin::configure(const ohp::PluginInfo& config, std::vector<std::string>& histograms)
{

    std::vector<std::string> lineArray;
        
    config.getParameters("lineArray",  lineArray);            
    config.getParameters("ALL",        m_layers["ALL"]);    
    config.getParameters("IBL",	       m_layers["IBL"]);               
    config.getParameters("IBLA",       m_layers["IBLA"]);          
    config.getParameters("IBLC",       m_layers["IBLC"]);             
    config.getParameters("L0",	       m_layers["L0"]);
    config.getParameters("L1",	       m_layers["L1"]);                
    config.getParameters("L2",	       m_layers["L2"]);
    config.getParameters("ECA",	       m_layers["ECA"]);               
    config.getParameters("ECC",        m_layers["ECC"]);
    config.getParameter("layers",      m_num_layers);
    config.getParameter("tablerow",    m_table_row);

    //m_tableLines = manipulateVerticalHeader(lineArray); !<< not used

    for(int i=0;i<m_num_layers;i++){
      if(int(m_layers[m_layer_name[i]].size()) !=m_table_row){
	ERS_LOG("ohpplugins::PixelTabMapPlugin::configure: same number of histograms required for matrix display,");
	ERS_LOG("ohpplugins::PixelTabMapPlugin::configure: please check again oks configuration.");
	return;
      }
    }
    
    for(int lines=0; lines< m_table_row; lines++)
      histograms.push_back(std::to_string(lines));

}

void ohpplugins::PixelTabMapPlugin::histogramUpdated(const std::string& name, bool show_masked)
{

    //Rows::iterator row = getRow(name);
    for(int l=0; l< m_num_layers; l++){
      std::string matrix_index = std::to_string(l)+ name;
      std::vector<std::string> tmp = m_layers[m_layer_name[l]];
      std::string histogram_name = tmp[std::stoi(name)];
      ERS_LOG("histogram name is " << histogram_name);

      //if(histogram_name == "EMPTY")
      //  m_dqmf_result[matrix_index]=Qt::white;
    
      ohp::CoreInterface& ci = ohp::CoreInterface::getInstance();
      
      TH2* histogram = static_cast<TH2*>(ci.getHistogram(histogram_name));
      if(!histogram || histogram_name!="EMPTY"){
	m_dqmf_result[matrix_index] =Qt::white;
	//ERS_LOG("ohp::PixelTabMap::histogramUpdated no histogram " << histogram_name);
	//ERS_LOG("Setting a white flag");
      }      
      
      const ohp::Histogram::Properties * properties = ohp::CoreInterface::getInstance().getHistogramProperties(histogram_name);
      
      if (!properties) {
	ohp::CoreInterface::getInstance().setProperty(histogram_name);
      
	std::vector<const ohp::Section*> opts =
	  ohp::CoreInterface::getInstance().getConfiguration().getMatchedOptions(histogram_name);

	for ( std::vector<const ohp::Section*>::const_iterator it = opts.begin();
	      it != opts.end(); ++it )
	  {
	    const ohp::Section* b = *it;
	    ERS_DEBUG(2, "Processing section '"<<b->m_name<<"' for the '"<<histogram_name<<"' histogram");
	    ohp::Options::const_iterator bit = b->m_options.begin();
	    ohp::Options::const_iterator end = b->m_options.end();
	    for ( ; bit != end; ++bit ) {
	      ohp::CoreInterface::getInstance().setProperty(histogram_name, bit->m_key, bit->m_values[0]);
	    }
	  }
	
	properties = ohp::CoreInterface::getInstance().getHistogramProperties(histogram_name);
	
      }
      
      if (!ohp::CoreInterface::getInstance().isOnline()) ERS_LOG("ohp::CoreInterface Not Online");      
      else {
	  ////////////////////////////////////////////////////////////////////////////////////////////////////////////                       
	  // DQMF                                                                                                                            
	  ////////////////////////////////////////////////////////////////////////////////////////////////////////////                       
	  //ERS_LOG("ohp::PixelTabMapPlugin:: trying to retrieve DQMF for histogram " << histogram_name);
	  std::string dqmf = properties->findFirst("DQMF");
	  if(dqmf.empty()){
	    //ERS_LOG("Failed retrieving dqmf for " << histogram_name);
	    m_dqmf_result[matrix_index] =Qt::white;
	  }
	  else {
	    try {
	      ISInfoDictionary dict(ohp::CoreInterface::getInstance().getPartition());
	      dqmf::is::Result res;
	      dict.getValue(dqmf, res);
	      if (res.status == dqmf::is::Result::Red) {
		m_dqmf_result[matrix_index] =Qt::red;
	      } else if (res.status == dqmf::is::Result::Yellow) {
		m_dqmf_result[matrix_index] =Qt::yellow;
	      } else if (res.status == dqmf::is::Result::Green) {
		m_dqmf_result[matrix_index] =Qt::green;
	      } else if (histogram) {
		m_dqmf_result[matrix_index] =Qt::gray;
	      } else  ERS_LOG("No DQMF status linked"); 
	    }
	    catch (daq::is::Exception & ex) {
	      ERS_DEBUG(2, "Error in retrieving DQMF status:" << ex);
	    }
	  }
	}
    }
}
