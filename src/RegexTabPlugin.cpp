/*
 File:		RegexTabPlugin.cpp
 Author:	andrea.dotti@cern.ch
 Modified:	Serguei Kolos
 */
#include <algorithm>

#include <TCanvas.h>
#include <TList.h>

#include <QScrollBar>
#include <QDebug>

#include <oh/OHIterator.h>

#include <ohp/HistogramPainter.h>

#include <ohp/CoreInterface.h>
#include <ohp/PluginBase.h>

#include <ohpplugins/RegexTabPlugin.h>

#include <ohpplugins/common/StatusBar.h>
#include <ohpplugins/common/TQCanvas.h>

namespace
{
    const char * const NAME = "RegexTabPlugin";
    const char * const LEGACY_NAME = "RegExpTabPlugin";
    OHP_REGISTER_PLUGIN(ohpplugins::RegexTabPlugin, NAME)
    OHP_REGISTER_PLUGIN(ohpplugins::RegexTabPlugin, LEGACY_NAME)
}
 
ohpplugins::RegexTabPlugin::RegexTabPlugin(const std::string& name)
  : PluginWidget(name, ohp::CANVAS),
    m_nx(0),
    m_ny(0),
    m_current_page(0)
{
    Ui::RegexTabPanel::setupUi(this);    
}

void 
ohpplugins::RegexTabPlugin::configure(const ohp::PluginInfo& config)
{
    config.getParameter("ndivx", m_nx);
    config.getParameter("ndivy", m_ny);
    config.getParameter("match", m_regex);
    
    StatusBar::getNXLabel()->setText(QString("nx: %1").arg(m_nx));
    StatusBar::getNYLabel()->setText(QString("ny: %1").arg(m_ny));
    StatusBar::getMessageLabel()->setText(m_regex.c_str());
    
    ohp::CoreInterface::getInstance().subscribe(m_regex, *this);
    
    config.getParameter("canvasWatcher", m_watcher_name);
}

void 
ohpplugins::RegexTabPlugin::histogramUpdated(const QString& histogram_name)
{
    ERS_DEBUG(0, "Histogram '"<<histogram_name.toStdString()<<"' is updated");
    drawHistogram(histogram_name.toStdString());
}

void 
ohpplugins::RegexTabPlugin::drawHistogram(const std::string& histogram_name)
{
    Histograms::iterator it = m_histograms.find(histogram_name);
    if (it == m_histograms.end()) {
    	it = m_histograms.insert(Histogram(histogram_name,m_histograms.size())).first;
        StatusBar::getNHistogramsLabel()->setText(QString("%1 Histograms").arg(m_histograms.size()));
    }
                                
    int pagenum = it->m_pad_id/(m_nx*m_ny);
    int padnum = it->m_pad_id%(m_nx*m_ny);
    if (pagenum == stackedCanvas->count()) {
    	addPage();
    }
    if (pagenum == m_current_page) {
        TQCanvas* canvas = (TQCanvas*)stackedCanvas->widget(pagenum);
        TPad* pad = canvas->getPad(padnum+1);
        pad->Clear();
        ERS_DEBUG(1, "Draw '"<<it->m_name<<"' histogram in the pad #"<<padnum
                    <<" on the page #" << pagenum << " of the '"<<getName()<<"' tab");
        ohp::histogram::drawWithConfig(getName(), it->m_name, pad);
        canvas->Refresh();
    }
}

void
ohpplugins::RegexTabPlugin::updateContent()
{
    m_histograms.clear();
    while (QWidget* w = stackedCanvas->currentWidget()) {
    	stackedCanvas->removeWidget(w);
    }
    
    try {
	IPCPartition p = ohp::CoreInterface::getInstance().getPartition();
	ohp::HistogramNameTokens tokens;
	if (!ohp::CoreInterface::tokenize(m_regex, tokens)) {
	    ERS_DEBUG(1, "Bad regular expression '"<<m_regex<<"' was given");
            return ;
	}
        OHIterator it(p, tokens.serverName, tokens.providerName, tokens.oldHistogramName);

	while (it()) {
	    std::string provider(it.provider());
	    std::string name(it.name());
	    if (name[0] != '/')
		name = "/" + name;
	    drawHistogram(tokens.serverName + "/" + provider + name);
	}
    }
    catch(daq::oh::Exception& ex) {
	ERS_DEBUG(0, ex);
    }

    TQCanvas* c = (TQCanvas*)stackedCanvas->currentWidget();
    if (c) {
    	c->Refresh();
    }
}

void 
ohpplugins::RegexTabPlugin::resumed()
{
    if (isVisible()) {
	updateContent();
    }
}

void
ohpplugins::RegexTabPlugin::on_stackedCanvas_currentChanged(int i)
{
    m_current_page = i;
    StatusBar::getNPagesLabel()->setText(
            QString("Page %1 of %2").arg(m_current_page+1).arg(stackedCanvas->count()));

    Histograms::iterator it = m_histograms.begin();
    for ( ; it != m_histograms.end(); ++it) {
        drawHistogram(it->m_name);
    }
}

void 
ohpplugins::RegexTabPlugin::addPage()
{
    TQCanvas* page = new TQCanvas(stackedCanvas);
    page->GetCanvas()->Divide(m_nx, m_ny);
    page->GetCanvas()->SetHighLightColor(10);
    page->setWatcher(QString::fromStdString(m_watcher_name));

    stackedCanvas->addWidget(page);
    verticalScrollBar->setMaximum(stackedCanvas->count()-1);
}

void 
ohpplugins::RegexTabPlugin::windowActivated()
{
    PluginWidget::windowActivated();
    StatusBar::getNXLabel()->setText(QString("nx: %1").arg(m_nx));
    StatusBar::getNYLabel()->setText(QString("ny: %1").arg(m_ny));
    StatusBar::getMessageLabel()->setText(m_regex.c_str());
    StatusBar::getNHistogramsLabel()->setText(QString("%1 Histograms").arg(m_histograms.size()));
    StatusBar::getNPagesLabel()->setText(QString("Page %1 of %2").arg(m_current_page+1).arg(stackedCanvas->count()));
}
