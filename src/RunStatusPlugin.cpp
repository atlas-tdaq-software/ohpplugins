/*
 File:		RunStatusPlugin.cpp
 Author:	andrea.dotti@cern.ch
 MODIFIED:	Roberto Agostino Vitillo [vitillo@cli.di.unipi.it] (Mid 2008) -- MDI support added
 MODIFIED:	Serguei Kolos
 */
#include <string>

#include <QLabel>
#include <QString>
#include <QLCDNumber>
#include <QPushButton>
#include <QLayout>
#include <QTextEdit>
#include <QGroupBox>

#include <ers/ers.h>
#include <TTCInfo/LumiBlock.h>
#include <rc/RunParams.h>
#include <rc/RCStateInfo.h>
#include <ohp/CoreInterface.h>

#include <ohpplugins/RunStatusPlugin.h>

namespace
{
    const char * const NAME = "RunStatusPlugin";
    OHP_REGISTER_PLUGIN(ohpplugins::RunStatusPlugin, NAME)

    void initStateColors(QMap<QString,QString>& colors)
    {
	colors.insert(QString("ABSENT"),     "gray");
	colors.insert(QString("UNKNOWN"),    "gray");
	colors.insert(QString("NONE"),	     "gray");
	colors.insert(QString("BOOTED"),     "lightGray");
	colors.insert(QString("INITIAL"),    "cyan");
	colors.insert(QString("CONFIGURED"), "yellow");
	colors.insert(QString("CONNECTED"),  "rgb(255, 200, 0)");
	colors.insert(QString("EFSTOPPED"),  "rgb(0, 210, 0)");
	colors.insert(QString("EBSTOPPED"),  "rgb(0, 210, 0)");
	colors.insert(QString("L2STOPPED"),  "rgb(0, 210, 0)");
	colors.insert(QString("L2SVSTOPPED"),"rgb(0, 210, 0)");
	colors.insert(QString("ROIBSTOPPED"),"rgb(0, 210, 0)");
	colors.insert(QString("SFOSTOPPED"), "rgb(0, 210, 0)");
	colors.insert(QString("GTHSTOPPED"), "rgb(0, 210, 0)");
	colors.insert(QString("READY"),	     "rgb(0, 210, 0)");
	colors.insert(QString("PAUSED"),     "rgb(0, 210, 0)");
	colors.insert(QString("RUNNING"),    "rgb(0, 210, 0)");
    }
}

ohpplugins::RunStatusPlugin::RunStatusPlugin(const std::string& name)
  : PluginWidget(name, ohp::STATUS)
{
    setupUi(this);
    
    initStateColors(m_state_colors);
        
    partitionname->setText(ohp::CoreInterface::getInstance().getPartition().name().c_str());
        
    if (!connect(this, SIGNAL(runNumber(int)), SLOT(setRunNumber(int)), Qt::QueuedConnection))
	ERS_LOG("Connect failed");
    if (!connect(this, SIGNAL(lbNumber(int)), SLOT(setLumiBlockNumber(int)), Qt::QueuedConnection))
        ERS_LOG("Connect failed");
    if (!connect(this, SIGNAL(runType(const QString&)), SLOT(setRunType(const QString&)), Qt::QueuedConnection))
	ERS_LOG("Connect failed");
    if (!connect(this, SIGNAL(runDate(const QString&)), SLOT(setRunDate(const QString&)), Qt::QueuedConnection))
	ERS_LOG("Connect failed");
    if (!connect(this, SIGNAL(runState(const QString&)), SLOT(setRunState(const QString&)), Qt::QueuedConnection))
	ERS_LOG("Connect failed");
        
    infrastructureUp();
}

void 
ohpplugins::RunStatusPlugin::runParamsReceiver(ISCallbackInfo * info)
{
    try {
	RunParams params;
	info->value(params);

	emit runNumber(params.run_number);
	emit runType(QString::fromStdString(params.run_type));
	emit runDate(QString::fromStdString(params.timeSOR.str()));
    }
    catch (daq::is::Exception & ex) {
	ERS_DEBUG(1, ex);
    }
}
    
void 
ohpplugins::RunStatusPlugin::lumiBlockReceiver(ISCallbackInfo * info)
{
    try {
        LumiBlock lb;
        info->value(lb);

        emit lbNumber(lb.LumiBlockNumber);
    }
    catch (daq::is::Exception & ex) {
        ERS_DEBUG(1, ex);
    }
}

void
ohpplugins::RunStatusPlugin::runControlReceiver(ISCallbackInfo * info)
{
    try {
	RCStateInfo rc;
	info->value(rc);
	emit runState(QString::fromStdString(rc.state));
    }
    catch ( daq::is::Exception & ex ) {
	ERS_DEBUG(1, ex);
    }
}

void 
ohpplugins::RunStatusPlugin::setRunNumber(int rn)
{
    runnumber->setNum(rn);
}

void 
ohpplugins::RunStatusPlugin::setLumiBlockNumber(int lb)
{
    lbnumber->setNum(lb);
}

void
ohpplugins::RunStatusPlugin::setRunType(const QString& s)
{
    runtype->setText(s);
}

void 
ohpplugins::RunStatusPlugin::setRunDate(const QString& d)
{
    rundate->setText(d);
}

void 
ohpplugins::RunStatusPlugin::setRunState(const QString& d)
{
    runstate->setText(d);
    QMap<QString,QString>::iterator it = m_state_colors.find(d);

    if (it != m_state_colors.end())
	runstate->setStyleSheet(QString("QLabel { background-color : %1;}").arg(it.value()));
    else
	runstate->setStyleSheet("QLabel { background-color : rgb(170, 170, 127);}");
}

void 
ohpplugins::RunStatusPlugin::infrastructureUp()
{
    ERS_DEBUG( 0, "Infrastructure is up, connecting to the IS servers");
    
    ISInfoDictionary dictionary(ohp::CoreInterface::getInstance().getPartition());
    try {
        RunParams params;
	dictionary.getValue("RunParams.RunParams", params);

	emit runNumber(params.run_number);
	emit runType(QString::fromStdString(params.run_type));
	emit runDate(QString::fromStdString(params.timeSOR.str()));
    }
    catch(daq::is::Exception& ex) {
	ERS_DEBUG(1, ex);
    }

    try {
	RCStateInfo rc;
	dictionary.getValue("RunCtrl.RootController", rc);
	emit runState(QString::fromStdString(rc.state));
    }
    catch ( daq::is::Exception & ex ) {
	ERS_DEBUG(1, ex);
    }

    m_receiver.reset(new ISInfoReceiver(ohp::CoreInterface::getInstance().getPartition()));
    try {
	m_receiver->subscribe("RunParams.RunParams",&ohpplugins::RunStatusPlugin::runParamsReceiver,this);
        m_receiver->subscribe("RunParams.LumiBlock",&ohpplugins::RunStatusPlugin::lumiBlockReceiver,this);
	m_receiver->subscribe("RunCtrl.RootController",&ohpplugins::RunStatusPlugin::runControlReceiver,this);
    }
    catch(daq::is::Exception& ex) {
	ERS_DEBUG(1, ex);
    }
}
