/*
 File:		StatusPlugin.cpp
 Author:	andrea.dotti@cern.ch
 MODIFIED:	Roberto Agostino Vitillo [vitillo@cli.di.unipi.it] (Mid 2008) -- MDI support added
 MODIFIED:	Serguei Kolos
 */
#include <string>

#include <QLabel>
#include <QString>
#include <QLCDNumber>
#include <QPushButton>
#include <QLayout>
#include <QTextEdit>
#include <QGroupBox>

#include <ers/ers.h>
#include <rc/RunParams.h>
#include <rc/RCStateInfo.h>
#include <ohp/CoreInterface.h>

#include <ohpplugins/StatusPlugin.h>

namespace
{
    const char * const NAME = "StatusPlugin";
    OHP_REGISTER_PLUGIN(ohpplugins::StatusPlugin, NAME)
}

ohpplugins::StatusPlugin::StatusPlugin(const std::string& name)
  : PluginWidget(name, ohp::STATUS),
    m_last_input_num(0)
{        
    setupUi(this);
    
    m_online_servers = ohp::CoreInterface::getInstance().getServers();
    updateServers(m_online_servers,onlServers);
            
    connect(&m_timer, SIGNAL(timeout()), SLOT(updateRates()));
    m_timer.start(1000); // 1 second
}

void 
ohpplugins::StatusPlugin::updateRates()
{
    int in = ohp::CoreInterface::getInstance().getInNotifications();
    innotif->display(in);

    int out = ohp::CoreInterface::getInstance().getOutNotifications();
    outnotif->display(out);
    
    int diff = in - m_last_input_num;
    m_last_input_num = in;
  
    rate->display(diff);      
}

void 
ohpplugins::StatusPlugin::updateServers(
	const std::vector<std::string> & servers, QLabel* widget)
{
    std::ostringstream out;
    std::copy(servers.begin(), servers.end(), std::ostream_iterator<std::string>(out, "\n"));
    std::string s(out.str());
    widget->setText(QString::fromLatin1(s.c_str(), s.size()-1));
}

void 
ohpplugins::StatusPlugin::serverUp(const QString& server)
{
    std::string s(server.toStdString());
    ERS_DEBUG(1, "'"<<s<<"' server up signal received");
    
    m_online_servers.push_back(s);
    m_offline_servers.erase(
	    std::find(	    m_offline_servers.begin(),
			    m_offline_servers.end(),
			    s));
    updateServers(m_online_servers,onlServers);
    updateServers(m_offline_servers,oflServers);
}

void 
ohpplugins::StatusPlugin::serverDown(const QString& server)
{
    std::string s(server.toStdString());
    ERS_DEBUG(1, "'"<<s<<"' server down signal received");
    
    m_offline_servers.push_back(s);
    m_online_servers.erase(
	    std::find(	    m_online_servers.begin(),
			    m_online_servers.end(),
			    s));
    updateServers(m_online_servers,onlServers);
    updateServers(m_offline_servers,oflServers);
}

void 
ohpplugins::StatusPlugin::paused()
{
    status->setText("PAUSED");
    status->setStyleSheet("QLabel { background-color : yellow; }");
}

void 
ohpplugins::StatusPlugin::resumed()
{
    status->setText("ACTIVE");
    status->setStyleSheet("QLabel { background-color : lightBlue; }");

}
