/*
 File:		TileCosmicsPlugin.cpp
 Author:	andrea.dotti@cern.ch
 Modified:	Serguei Kolos
 */
#include <algorithm>
#include <boost/algorithm/string.hpp>

#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <TLatex.h>
#include <TStyle.h>

#include <ohp/HistogramPainter.h>

#include <ohp/CoreInterface.h>
#include <ohp/PluginBase.h>

#include <ohpplugins/TileCosmicsPlugin.h>

#include <ohpplugins/common/StatusBar.h>

#include <QLabel>
#include <QApplication>
#include <QPushButton>
#include <QRadioButton>
#include <QEvent>
#include <QProgressBar>
#include <QEventLoop>
#include <QSpinBox>

namespace
{
    const char * const NAME = "TileCosmicsPlugin";
    OHP_REGISTER_PLUGIN(ohpplugins::TileCosmicsPlugin, NAME)

    /** Implements the ordering of two Sample histograms
            returns true if Energy of h1 (simil-FlatFilter) > h2
            Energy calculated as sum of Sum_{i=1}^{i=8}S[i]-9*S[0]
    */
    bool SuperFlatFilter(TH1* const & h1, TH1* const & h2)
    {
	Double_t ih1 = h1->Integral() - (h1->GetNbinsX()*h1->GetBinContent(1));
	Double_t ih2 = h2->Integral() - (h2->GetNbinsX()*h2->GetBinContent(1));
	return ih1 > ih2;
    }
    
    /** Implements ordering of two PMTs based on highest sample in fifth sample
            returns true if S[4]_h1>S[4]_h2
	Bin 0 is underflow. Samples start counting from 0, so fifth sample is bin 6
    */
    bool FifthSample(TH1* const & h1, TH1* const & h2)
    {
	return (h1->GetBinContent(6) - h1->GetBinContent(1)) 
        		> (h2->GetBinContent(6) - h2->GetBinContent(1));
    }
    
    /** Implements ordering of two PMTs based on Sum of samples third, fourth, fifth samples
            Pedestal (S[0]) is subtracted
            returns true if (S[3]+S[4]+S[5])_h1>(S[3]+S[4]+S[6])_h2
    */
    bool SumThreeSamples(TH1* const & h1, TH1* const & h2)
    {
	Double_t ih1 = h1->GetBinContent(4) + h1->GetBinContent(5) + h1->GetBinContent(6) 
        		- (3*h1->GetBinContent(1));
	Double_t ih2 = h2->GetBinContent(4) + h2->GetBinContent(5) + h2->GetBinContent(6) 
        		- (3*h2->GetBinContent(1));
	return ih1 > ih2;
    }
    
    /** Implements ordering of two PMTs based on Flat Filter method.
            FF(h) is the max{ sum of 5 samples } 1<=S<NSAMPLES. S[0] is pedestal 
            returns true if FF(h1) > FF(h2)
    */
    bool FlatFilter(int NSAMPLES, TH1* const & h1, TH1* const & h2)
    {
	Double_t maxh1 = -10000, ih1 = 0;
	Double_t maxh2 = -10000, ih2 = 0;
	for ( Int_t startbin = 2; startbin <= NSAMPLES; ++startbin) {
	    for ( Int_t bin = startbin; bin < startbin + 5; ++bin) {
		ih1 += h1->GetBinContent(bin);
		ih2 += h1->GetBinContent(bin);
	    }
	    ih1 -= (h1->GetBinContent(1)*5);
	    ih2 -= (h2->GetBinContent(1)*5);
	    if ( ih1 > maxh1 ) maxh1=ih1;
	    if ( ih2 > maxh2 ) maxh2=ih2;
	}
	return ih1 > ih2;
    }
    
    /** Implemnets ordering of two PMT's based on Flat Filter method.
            FF(h) is the max{ sum of 5 samples } 2<=S<NSAMPLES. (S[0]+S[1])/2 is the pdestal
            returns true if FF(h1) > FF(h2)
    */
    bool FlatFilterPedestalFirstTwo(int NSAMPLES, TH1* const & h1, TH1* const & h2)
    {
	Double_t maxh1 = -10000, ih1 = 0;
	Double_t maxh2 = -10000, ih2 = 0;
	for ( Int_t startbin = 3; startbin <= NSAMPLES; ++startbin) {
	    for ( Int_t bin = startbin; bin < startbin + 5; ++bin) {
	      ih1 += h1->GetBinContent(bin);
	      ih2 += h1->GetBinContent(bin);
	    }
	    ih1 -= ( (h1->GetBinContent(1)+h1->GetBinContent(2))/2*5);
	    ih2 -= ( (h2->GetBinContent(1)+h1->GetBinContent(2))/2*5);
	    if ( ih1 > maxh1 ) maxh1=ih1;
	    if ( ih2 > maxh2 ) maxh2=ih2;
	}
	return ih1 > ih2;
    }
}

ohpplugins::TileCosmicsPlugin::TileCosmicsPlugin(const std::string& name)
  : PluginWidget(name, ohp::CANVAS),
    m_template_name("/EXPERT/_module_/Samples/PMT_pmtnum_"),
    m_events(0)
{    
    setupUi(this);        
    setDefaults();
}

void 
ohpplugins::TileCosmicsPlugin::configure(const ohp::PluginInfo& config)
{
    std::string s("TileLBA-ROS0/EXPERT/LBA01/Samples/PMT1");

    config.getParameter("subscription", s);
    std::vector<std::string> histograms;
    histograms.push_back(s);
    setWatchedHistograms(histograms);

    config.getParameter("basepath", m_template_name);

    std::string o;
    config.getParameter("ordering", o);
    std::transform(o.begin(), o.end(), o.begin(), ::tolower);
    setOrdering(o);

    std::vector<std::string>  modules;
    config.getParameters("modules", m_modules);

    std::vector<std::string>  providersmap;
    config.getParameters("providermap", providersmap);
    
    if (!providersmap.empty()) {
    	m_providers.clear();
    }
    
    for ( std::vector<std::string>::iterator it = providersmap.begin();
	    it != providersmap.end(); ++it )
    {
	std::string elem = *it;
	std::string::size_type loc = elem.find(":",0);
	if ( loc != std::string::npos ) { 
	    std::string m(elem,0,loc);
	    std::string p(elem,loc+1);
	    m_providers.insert(std::make_pair(m,p));
	}
    }    
}

void 
ohpplugins::TileCosmicsPlugin::setDefaults()
{
    std::string parts[4] = {"LBA", "LBC", "EBA", "EBC"};
    for ( int p = 0; p < 4; ++p ) {
	for (int m = 1; m<=64; ++m) {
	    std::stringstream mm;
	    mm<<parts[p]<<std::setfill('0')<<std::setw(2)<<m;
	    m_modules.push_back(mm.str());
	    
            std::string provider = "TileGNAMMon_Tile" + parts[p] + "-ROS";
	    provider += ( m <=32 ) ? '0' : '1';
	    m_providers[mm.str()] = provider;
	}
    }
}

void 
ohpplugins::TileCosmicsPlugin::setOrdering(const std::string& type)
{
    if      ( type == "samplesum")			{ m_ordering = SuperFlatFilter; }
    else if ( type == "fifthsample" )			{ m_ordering = FifthSample; }
    else if ( type == "threesamples" )			{ m_ordering = SumThreeSamples; }
    else if ( type == "flatfilter7" )			{ m_ordering = boost::bind(FlatFilter,3,_1,_2); }
    else if ( type == "flatfilter9" )			{ m_ordering = boost::bind(FlatFilter,5,_1,_2); }
    else if ( type == "flatfilter7twosampleped" )	{ m_ordering = boost::bind(FlatFilterPedestalFirstTwo,7,_1,_2); }
    else if ( type == "flatfilter9twosampleped" )	{ m_ordering = boost::bind(FlatFilterPedestalFirstTwo,9,_1,_2); }
    else 						{ m_ordering = SuperFlatFilter; }
}

void 
ohpplugins::TileCosmicsPlugin::on_autoDisplay_toggled(bool state)
{
    if (state) {
	subscribe(getWatchedHistograms());
	updateContent();
    }
}

void 
ohpplugins::TileCosmicsPlugin::on_showFirst_toggled(bool state)
{
    showLast->setChecked(!state);
}

void 
ohpplugins::TileCosmicsPlugin::on_showLast_toggled(bool state)
{
    showFirst->setChecked(!state);
}

void 
ohpplugins::TileCosmicsPlugin::setVisisble(bool state)
{
    if (!state || autoDisplay->isChecked()) {
    	PluginWidget::setVisible(state);
    }
    else {
	QWidget::setVisible(state);
    }
}

void 
ohpplugins::TileCosmicsPlugin::histogramUpdated(const QString& )
{
    updateContent();
}

void
ohpplugins::TileCosmicsPlugin::updateContent()
{
    theCanvas->GetCanvas()->Clear();
    theCanvas->cd();
    TLatex* msg = new TLatex(0.4, 0.4, "Please wait..." );
    msg->Draw();
    theCanvas->Refresh();
    
    progressBar->setRange(0, m_modules.size());

    std::list<TH1*> histograms;
    for (size_t i = 0; i < m_modules.size(); ++i)
    {
	// Change _module_ in the base histogram name with the real module name
	std::string histogram_name(boost::replace_first_copy(m_template_name, "_module_", m_modules[i]));

	// Search which provider is responsible for this module
	ProvidersMap::iterator it = m_providers.find(m_modules[i]);
	if (it != m_providers.end()) {
	    histogram_name = it->second + histogram_name;
	    for (int pmt = 1; pmt <= 48; ++pmt) {
                boost::replace_first(histogram_name, "_pmtnum_", boost::lexical_cast<std::string>(pmt));                
		TObject* h = ohp::CoreInterface::getInstance().getHistogram(histogram_name);
		if (h) {
		    histograms.push_back((TH1*)h);
		}
	        QCoreApplication::processEvents(
	                QEventLoop::ExcludeUserInputEvents | QEventLoop::ExcludeSocketNotifiers, 10);
	    }
	}
	progressBar->setValue(i+1);
    }
    StatusBar::getNHistogramsLabel()->setText(QString("# of events = %1").arg(++m_events));
    
    histograms.sort(m_ordering);
    
    drawList(histograms);    
}

void 
ohpplugins::TileCosmicsPlugin::drawList(const std::list<TH1*>& histograms)
{
    theCanvas->GetCanvas()->Clear();
    
    int max_histograms_num = std::min((int)histograms.size(), (int)spinBox->value());
    int nx = ::sqrt(max_histograms_num);
    int ny = nx ? max_histograms_num / nx : 0;

    if (nx*ny < max_histograms_num) {
	++nx;
    }

    theCanvas->GetCanvas()->Clear();
    theCanvas->GetCanvas()->Resize();
    theCanvas->GetCanvas()->Divide(nx,ny);
    
    if (showFirst->isChecked()) {
	// Draw most energetic PMTs 
	int pad = 1;
	for (std::list<TH1*>::const_iterator it = histograms.begin(); 
        	it != histograms.end() && pad <= max_histograms_num; 
                ++it, ++pad)
        {
	    theCanvas->cd(pad);
	    /// Set a reasonable scale in case of noise...
	    Double_t min = 1024;
            Double_t max = 0;
	    for (int samp = 1; samp <= (*it)->GetNbinsX(); ++samp) {
		Double_t b = (*it)->GetBinContent(samp);
		min = std::min(min,b);
		max = std::max(max,b);
	    }
	    if ((max-min) < 20) {
            	(*it)->GetYaxis()->SetRangeUser(min-2,min+22);
            }
	    (*it)->Draw();
	}
    }
    else // Draw less energetic PMTs
    {
	int pad = 1;
	for ( std::list<TH1*>::const_reverse_iterator it = histograms.rbegin(); 
        	it != histograms.rend() && pad <= max_histograms_num; 
                ++it, ++pad)
        {
	    theCanvas->GetCanvas()->cd(pad);
	    (*it)->Draw();
	}
    }
    theCanvas->Refresh();
}

void 
ohpplugins::TileCosmicsPlugin::resumed()
{
    if (isVisible()) {
	updateContent();
    }
}
