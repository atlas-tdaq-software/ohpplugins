/*
  File:		TileNoisePlugin.cpp
  Author:	Roberto Agostino Vitillo [vitillo@cern.ch]
  Modified:	Serguei Kolos
*/

#include <ohpplugins/TileNoisePlugin.h>

#include <ohpplugins/common/TablePlugin.h>

#include <ohp/CoreInterface.h>

#include <TH2.h>
namespace
{
    const char * const NAME = "TileNoisePlugin";
    OHP_REGISTER_PLUGIN(ohpplugins::TablePlugin<ohpplugins::TileNoisePlugin>, NAME)

    QVariant headers[] = { "Module", "Channel", "Value" };
}

ohpplugins::TileNoisePlugin::TileNoisePlugin()
  : TableModel(headers),
    m_threshold(0)
{ ; }

void 
ohpplugins::TileNoisePlugin::configure(const ohp::PluginInfo& config, std::vector<std::string>& histograms)
{
    config.getParameter ("threshold", m_threshold);

    std::vector<std::string> names;
    std::vector<std::string> providers;
    std::string pattern;
        
    config.getParameter ("template",	pattern);
    config.getParameters("nameset",	names);
    config.getParameters("providers",	providers);

    std::vector<std::string>::const_iterator name     = names.begin();
    std::vector<std::string>::const_iterator provider = providers.begin();

    std::string::size_type pos = pattern.find("*");
    std::string pre  = pattern.substr(0, pos);
    std::string post = pattern.substr(pos+1);

    while(provider != providers.end()) {
	while(name != names.end()) {
	    if(*name == "*") {
		++name;
		break;
	    }
	    histograms.push_back( *provider + pre + *name + post + *name);
	    ++name;
	}
	++provider;
    }
}

void 
ohpplugins::TileNoisePlugin::histogramUpdated(const std::string& name, bool show_masked)
{
    ohp::CoreInterface& ci = ohp::CoreInterface::getInstance();
    
    TH2* histogram = static_cast<TH2*>(ci.getHistogram(name));
    if (!histogram)
	return;
    
    for(int x = 1; x <= histogram->GetXaxis()->GetNbins(); x++) {
        std::string key = name + QString().setNum(x).toStdString();
        
	Double_t value = histogram->GetBinContent(x);
	if(value >= m_threshold) {
	    Rows::iterator row = getRow(key);
            
	    std::string prefix = name;
	    prefix.erase(prefix.rfind('/'));
	    prefix.erase(prefix.rfind('/'));
            
	    if(show_masked || !m_tile_helper.checkCH(x, prefix)) {
		row->m_cells[MODULE] = QString::fromStdString(prefix);
		row->m_cells[PMT] = QString().setNum(x-1);
		row->m_cells[VALUE] = QString().setNum(value);
	    }
	}
        else {
	    removeRow(key);
	}
    }
    
    delete histogram;
}
