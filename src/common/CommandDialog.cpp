/*
  File:		CommandDialog.cpp
  Author:	andrea.dotti@cern.ch
  Modified:	Serguei Kolos
 */
#include <ohpplugins/common/CommandDialog.h>

#include <ohp/CoreInterface.h>

#include <QMessageBox>

#include <string>

#include <ers/ers.h>

ohpplugins::CommandDialog::CommandDialog(QWidget* parent) 
  : QDialog(parent)
{
    setupUi(this);
    
    commands->addItem("Reset");
    commands->addItem("Rebin");
}

void 
ohpplugins::CommandDialog::setHistogramName(const QString& histogram_name)
{
    m_full_name = histogram_name;

    std::string n(m_full_name.toStdString());
    ohp::HistogramNameTokens tokens;
    if (ohp::CoreInterface::tokenize(n,tokens)) {
    	std::string sp = tokens.serverName + "/" + tokens.providerName;
    	m_server_provider = sp.c_str();
        providerRadioButton->setEnabled(true);
    }
    
    if ( histogramRadioButton->isChecked() ) {
	targetLabel->setText(m_full_name);
    }
    else {
	targetLabel->setText(m_server_provider);
    }
}

void 
ohpplugins::CommandDialog::on_histogramRadioButton_toggled(bool state)
{
    if (state) {
	targetLabel->setText(m_full_name);
    }
}

void 
ohpplugins::CommandDialog::on_providerRadioButton_toggled(bool state)
{
    if (state) {
	targetLabel->setText(m_server_provider);
    }
}

void 
ohpplugins::CommandDialog::on_sendButton_clicked()
{
    QString command = commands->currentText();
    QString params  = parameters->currentText();
    
    if (!params.isEmpty()) {
    	command += "|" + params;
    }

    std::string target(targetLabel->text().toStdString());
    std::string cmd(command.toStdString());
    
    bool ok = ohp::CoreInterface::getInstance().sendCommand(
    			target, cmd, providerRadioButton->isChecked());

    if(!ok) {
	QMessageBox::critical( this, "Command sending failure!", 
		QString::fromStdString("Failed to send '" + cmd + "' command to the '" + target + "' object"),
		QMessageBox::Ok);
    }
    else {    
    	emit commandSent(targetLabel->text(), command);
	commands->addItem(commands->currentText());
	parameters->addItem(parameters->currentText());
	hide();
    }
}
