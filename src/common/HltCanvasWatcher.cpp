/*
 File:		HltCanvasWatcher.cpp
 Modified:	Serguei Kolos
 */
#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>

#include <ohpplugins/common/HltCanvasWatcher.h>
#include <ohpplugins/common/TQCanvas.h>

#include <ohp/CoreInterface.h>
#include <ohp/Configuration.h>

#include <QEvent>

#include <TH2.h>
#include <TList.h>

ohpplugins::HltCanvasWatcher::HltCanvasWatcher(QObject * parent)
  : ohpplugins::TQCanvasWatcher(parent)
{ ; }

bool 
ohpplugins::HltCanvasWatcher::mouseHandler(QObject* qobj, QMouseEvent* evt)
{
    if (   evt->type()   != QEvent::MouseButtonDblClick
    	|| evt->button() != Qt::LeftButton) 
    {
	return TQCanvasWatcher::mouseHandler(qobj, evt);
    }

    TQCanvas* widget = (TQCanvas*)qobj;
    if (!widget) return true;

    TCanvas* canvas = widget->GetCanvas();
    if (!canvas) return true;

    TVirtualPad* pad = canvas->Pick(evt->x(), evt->y(), 0);
    if(pad == 0) return true;

    float x = pad->AbsPixeltoX(evt->x());
    float y = pad->AbsPixeltoY(evt->y());

    TList* list = pad->GetListOfPrimitives();
    TIter it(list);

    TObject* tobj = 0;
    while (TObject* o = it()) {
	if (o->IsA() == TH2F::Class()) {
	    tobj = o;
	    break;
	}
    }

    if (!tobj || !processHistogram((TH2F*)tobj,x,y)) {
	return TQCanvasWatcher::mouseHandler(qobj, evt);
    }
    return true;
}

bool
ohpplugins::HltCanvasWatcher::processHistogram(TH2F* tobj, float x, float y)
{
    std::string name = tobj->GetName();
    const ohp::Configuration& config = ohp::CoreInterface::getInstance().getConfiguration();
    const ohp::Section* options  = config.getGlobalOptions(name);
    
    if (!options) {
    	return false;
    }
    
    Double_t zoom_x_size = 0.4;
    Double_t zoom_y_size = 0.4;
    std::vector<std::string> relatives;
    for (ohp::Options::iterator it = options->m_options.begin(); 
    		it != options->m_options.end(); ++it)
    {
	if (it->m_key.find("RELATED_HISTS") == 0) {
	    typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
	    boost::char_separator<char> separators(" \t\n");

	    tokenizer tokens(it->m_values[0], separators);
	    for (tokenizer::iterator it = tokens.begin(); it != tokens.end(); ++it)
	    {
		relatives.push_back(*it);
	    }
	}
	else if (it->m_key == "ZOOM_X_SIZE") {
	    zoom_x_size = boost::lexical_cast<Double_t>(it->m_values[0]);
	}
	else if (it->m_key == "ZOOM_Y_SIZE") {
	    zoom_y_size = boost::lexical_cast<Double_t>(it->m_values[0]);
	}
    }
    
    if (relatives.empty()) {
    	return false;
    }
    
    TH2F* clone = (TH2F*)ohp::CoreInterface::getInstance().cloneObject(tobj);

    double xmin = std::max(x - zoom_x_size, clone->GetXaxis()->GetXmin());
    double xmax = std::min(x + zoom_x_size, clone->GetXaxis()->GetXmax());
    double ymin = std::max(y - zoom_y_size, clone->GetYaxis()->GetXmin());
    double ymax = std::min(y + zoom_y_size, clone->GetYaxis()->GetXmax());

    // Set the histogram to the window required
    clone->SetAxisRange(xmin, xmax, "X");
    clone->SetAxisRange(ymin, ymax, "Y");

    // Get histograms representing the spot
    std::vector<TH2F*> histograms;
    for (std::vector<std::string>::iterator it = relatives.begin();
    		it != relatives.end(); ++it)
    {
	TH2F* h = (TH2F*)ohp::CoreInterface::getInstance().getHistogram(*it);
	if ( !h ) {
	    continue;
	}
	double this_xmin = h->GetXaxis()->GetXmin();
	double this_xmax = h->GetXaxis()->GetXmax();
	double this_ymin = h->GetYaxis()->GetXmin();
	double this_ymax = h->GetYaxis()->GetXmax();
	if (	    this_xmax >= xmin && this_xmin <= xmax
		 && this_ymax >= ymin && this_ymin <= ymax)
	{
	    double rxmin = std::max(x - zoom_x_size, this_xmin);
	    double rxmax = std::min(x + zoom_x_size, this_xmax);
	    double rymin = std::max(y - zoom_y_size, this_ymin);
	    double rymax = std::min(y + zoom_y_size, this_ymax);
	    h->SetAxisRange(rxmin, rxmax, "X");
	    h->SetAxisRange(rymin, rymax, "Y");
	    histograms.push_back(h);
	}
    }
    
    TQCanvas* popup = new TQCanvas();
            
    popup->setAttribute(Qt::WA_DeleteOnClose, true);
    
    TCanvas* canvas = popup->GetCanvas();
    
    if (histograms.size() > 2)
	if (histograms.size() > 6)
	    canvas->Divide((histograms.size() + 3) / 4, 4);
	else
	    canvas->Divide((histograms.size() + 1) / 2, 2);
    else
	canvas->Divide(1, 2);
        
    canvas->Update();
    canvas->cd(1);
    
    clone->Draw("COLZ");
    for (size_t i = 0; i < histograms.size(); i++) {
	canvas->cd(2 + i);
        histograms[i]->Draw("COLZ");
    }
    canvas->Update();
    
    popup->show();
    return true;
}
