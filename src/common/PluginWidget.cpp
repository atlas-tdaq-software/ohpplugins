/*
 File:		PluginWidget.cpp
 Author:	Serguei Kolos
 */

#include <QEvent>
#include <QDebug>

#include <ohpplugins/common/PluginWidget.h>

void 
ohpplugins::PluginWidget::subWindowActivated(QMdiSubWindow* sw)
{
    if (!sw) {
    	return ;
    }
    
    if (ohpplugins::PluginWidget *w = qobject_cast<ohpplugins::PluginWidget*>(sw->widget())) {
    	if (w == this) {
	    windowActivated();
        }
    }
}

void 
ohpplugins::PluginWidget::windowActivated() 
{
    StatusBar::getMouseTrackingLabel()->clear();
    StatusBar::getObjectNameLabel()->clear();
    StatusBar::getMessageLabel()->clear();
    StatusBar::getNXLabel()->clear();
    StatusBar::getNYLabel()->clear();
    StatusBar::getNPagesLabel()->clear();
    StatusBar::getNHistogramsLabel()->clear();
}

void
ohpplugins::PluginWidget::customEvent(QEvent* e)
{
    switch (static_cast<ohp::Event::Type>(e->type())) {
	case ohp::Event::HistogramUpdated:
	    histogramUpdated(static_cast<ohp::Event*>(e)->m_data);
	    break;

	case ohp::Event::InfrastructureUp:
	    infrastructureUp();
	    break;

	case ohp::Event::InfrastructureDown:
	    infrastructureDown();
	    break;

	case ohp::Event::ServerUp:
	    serverUp(static_cast<ohp::Event*>(e)->m_data);
	    break;

	case ohp::Event::ServerDown:
	    serverDown(static_cast<ohp::Event*>(e)->m_data);
	    break;

	case ohp::Event::Paused:
	    paused();
	    break;

	case ohp::Event::Resumed:
	    resumed();
	    break;

	default:
	    break;
    }
}
