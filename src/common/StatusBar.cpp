/*
  File:		StatusBar.cpp
  Author:	Serguei Kolos
*/

#include <ohpplugins/common/StatusBar.h>

QStatusBar* ohpplugins::StatusBar::m_widget;

namespace
{
    QLabel* makeLabel()
    {
    	QLabel* w = new QLabel();
        w->setFrameShape(QFrame::Panel);
        w->setFrameShadow(QFrame::Sunken);
        return w;
    }
}

void 
ohpplugins::StatusBar::setWidget(QStatusBar* widget)
{
    m_widget = widget;
    m_widget->addPermanentWidget(getMouseTrackingLabel());
    m_widget->addPermanentWidget(getObjectNameLabel());
    m_widget->addPermanentWidget(getMessageLabel(), 1);	// takes all unused space
    m_widget->addPermanentWidget(getNXLabel());
    m_widget->addPermanentWidget(getNYLabel());
    m_widget->addPermanentWidget(getNPagesLabel());
    m_widget->addPermanentWidget(getNHistogramsLabel());
    
}

QLabel* 
ohpplugins::StatusBar::getMouseTrackingLabel()
{
    static QLabel* w = makeLabel();
    
    return w;
}

QLabel* 
ohpplugins::StatusBar::getObjectNameLabel()
{
    static QLabel* w = makeLabel();
        
    return w;
}

QLabel* 
ohpplugins::StatusBar::getNXLabel()
{
    static QLabel* w = makeLabel();
        
    return w;
}

QLabel* 
ohpplugins::StatusBar::getNYLabel()
{
    static QLabel* w = makeLabel();
        
    return w;
}
        
QLabel* 
ohpplugins::StatusBar::getNPagesLabel()
{
    static QLabel* w = makeLabel();
        
    return w;
}

QLabel* 
ohpplugins::StatusBar::getNHistogramsLabel()
{
    static QLabel* w = makeLabel();
        
    return w;
}

QLabel* 
ohpplugins::StatusBar::getMessageLabel()
{
    static QLabel* w = makeLabel();
        
    return w;
}
