/*
  File:		TQCanvas.cpp
  Author:	Serguei Kolos
*/

#include <QDebug>
#include <QPainter>

#include <TSystem.h>
#include <TVirtualX.h>

#include <ohpplugins/common/TQCanvas.h>
#include <ohpplugins/common/HltCanvasWatcher.h>


using namespace ohpplugins;

TQCanvas::TQCanvas(QWidget* parent, int w, int h)
  : QWidget(parent),
    m_canvas(nullptr),
    m_watcher(new TQCanvasWatcher(this)),
    m_size(w, h)
{
    //setAttribute(Qt::WA_PaintOnScreen, true);
    setAttribute(Qt::WA_OpaquePaintEvent, true);
    setUpdatesEnabled(false);
    setMouseTracking(true);

    int wid = gVirtualX->AddWindow((ULong_t)winId(), w, h);
    m_canvas = new TCanvas("Root Canvas", width(), height(), wid);

    if (parent) {
    	installEventFilter(m_watcher.data());
    }
}
    
QSize 
TQCanvas::sizeHint() const
{
    return m_size;
}

void 
TQCanvas::setWatcher(const QString& watcher_name)
{
    if (watcher_name == "HltCanvasWatcher") {
	setWatcher(new HltCanvasWatcher(this));
    }
}

void 
TQCanvas::setWatcher(TQCanvasWatcher * watcher)
{
    m_watcher = QSharedPointer<TQCanvasWatcher>(watcher);
    installEventFilter(m_watcher.data());
}

void
TQCanvas::Clear()
{ 
    if (m_canvas) {
        m_canvas->Clear();
        Refresh();
    }
}

void
TQCanvas::Refresh()
{ 
    if (m_canvas) {
        m_canvas->Resize();
        m_canvas->Update();
    }
}

TPad* 
TQCanvas::getPad(int padid)
{ 
    return (TPad*)GetCanvas()->GetPad(padid);
}

void TQCanvas::mouseMoveEvent(QMouseEvent *e)
{
    if (e->buttons() & Qt::LeftButton) {
        m_canvas->HandleInput(kButton1Motion, e->x(), e->y());
    }
    else if (e->buttons() & Qt::MiddleButton) {
        m_canvas->HandleInput(kButton2Motion, e->x(), e->y());
    }
    else if (e->buttons() & Qt::RightButton) {
        m_canvas->HandleInput(kButton3Motion, e->x(), e->y());
    }
    else {
        m_canvas->HandleInput(kMouseMotion, e->x(), e->y());
    }
}

void TQCanvas::mousePressEvent(QMouseEvent *e)
{
    switch (e->button()) {
        case Qt::LeftButton:
            m_canvas->HandleInput(kButton1Down, e->x(), e->y());
            break;
        case Qt::MiddleButton:
            m_canvas->HandleInput(kButton2Down, e->x(), e->y());
            break;
        case Qt::RightButton:
            m_canvas->HandleInput(kButton3Down, e->x(), e->y());
            break;
        default:
            break;
    }
}

void TQCanvas::mouseReleaseEvent(QMouseEvent *e)
{
    switch (e->button()) {
        case Qt::LeftButton:
            m_canvas->HandleInput(kButton1Up, e->x(), e->y());
            break;
        case Qt::MiddleButton:
            m_canvas->HandleInput(kButton2Up, e->x(), e->y());
            break;
        case Qt::RightButton:
            m_canvas->HandleInput(kButton3Up, e->x(), e->y());
            break;
        default:
            break;
    }
}

void TQCanvas::changeEvent(QEvent * )
{
    Refresh();
}

void TQCanvas::paintEvent(QPaintEvent * )
{
    Refresh();
}

void TQCanvas::resizeEvent(QResizeEvent * e)
{
    Refresh();
}

void TQCanvas::showEvent(QShowEvent * )
{
    Refresh();
}
