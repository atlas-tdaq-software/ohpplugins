/*
 File:		TQCanvasWatcher.cpp
 Author:	andrea.dotti@cern.ch
 Modified:	Serguei Kolos
 */
#include <string>

#include <TClass.h>
#include <TList.h>
#include <TStyle.h>
#include <TH1.h>
#include <TEfficiency.h>

#include <QEvent>
#include <QLabel>
#include <QToolTip>
#include <QDebug>

#include <ohp/CoreInterface.h>

#include <ohpplugins/common/TQCanvas.h>
#include <ohpplugins/common/StatusBar.h>
#include <ohpplugins/common/TQCanvasWatcher.h>

using namespace std;

ohpplugins::TQCanvasWatcher::TQCanvasWatcher(QObject* parent) 
  : QObject(parent)
{ ; }

bool 
ohpplugins::TQCanvasWatcher::eventFilter(QObject* obj, QEvent * evt)
{
    if ( evt->type() == QEvent::MouseButtonDblClick ||
	 evt->type() == QEvent::MouseButtonPress ||
	 evt->type() == QEvent::MouseButtonRelease ||
	 evt->type() == QEvent::DragMove ||
	 evt->type() == QEvent::MouseMove)
    {
	return mouseHandler(obj, (QMouseEvent*) evt);
    }
    
    return false;
}

TObject* 
ohpplugins::TQCanvasWatcher::findHistogram(TVirtualPad* pad) const
{
    if(pad == 0) return 0;
    
    TList* list = pad->GetListOfPrimitives();
    TIter it(list);
    while (TObject* obj = it()) {
	if (	   obj->IsA()->InheritsFrom("TH1")
		|| obj->IsA()->InheritsFrom("TGraph")
		|| obj->IsA()->InheritsFrom("TGraph2D")
                || obj->IsA()->InheritsFrom("TEfficiency"))
	{
	    QString name(obj->GetName());
	    if (!name.startsWith("reference:")) {
		return obj;
            }
	}
    }
    return 0;
}

TVirtualPad* 
ohpplugins::TQCanvasWatcher::getPad(QObject* qobj, QMouseEvent* evt)
{
    TQCanvas* widget = (TQCanvas*)qobj;
    if (!widget) return 0;

    TCanvas* canvas = widget->GetCanvas();
    if (!canvas) return 0;

    TVirtualPad* pad = canvas->Pick(evt->x(), evt->y(), 0);
    if(pad == 0) return 0;
    
    return pad;
}

void 
ohpplugins::TQCanvasWatcher::middleButtonPressed(QObject* qobj, QMouseEvent* evt)
{
    TVirtualPad* pad = getPad(qobj, evt);
    if(pad == 0) {
    	return ;
    }

    TObject* obj = pad->GetSelected();

    if (obj && 
    	(obj->IsA()->InheritsFrom("TH1")||
	 obj->IsA()->InheritsFrom("TGraph")||
	 obj->IsA()->InheritsFrom("TGraph2D")||
         obj->IsA()->InheritsFrom("TEfficiency")))
    {
	m_command_dialog.setHistogramName(obj->GetName());
	m_command_dialog.exec();
    }
}

bool 
ohpplugins::TQCanvasWatcher::mouseHandler(QObject* qobj, QMouseEvent* evt)
{
    TVirtualPad* pad = getPad(qobj, evt);
    if(pad == 0) {
    	return true;
    }

    /*
     Some mouse events for TQCanvas are non standard in ohp. 
     The main problem is that ohp works using asynchronous notifications.
     While a user is interacting with a histogram the histogram may change
     due to an external notification.
     We have to handle this and extend the basic behavior of the TQCanvas.
     */
    switch (evt->type()) {
	case QEvent::MouseMove: 
        {
	    TObject* selection = pad->GetSelected();
            if (selection && selection->IsA()->InheritsFrom("TH1")) {
		Double_t x = pad->AbsPixeltoX(evt->x());
                Double_t y = pad->AbsPixeltoY(evt->y());
                TH1* h = (TH1*)selection;
                int b = h->FindBin(x,y);
                StatusBar::getMouseTrackingLabel()->setText(
                	QString("pad #%1: bin[%2] = %3").arg(pad->GetNumber()).arg(b).arg(h->GetBinContent(b)));
		
                QString name(h->GetName());
                if (StatusBar::getObjectNameLabel()->text() != name) {
		    std::string documentation =
			ohp::CoreInterface::getInstance().getHistogramDocumentation(h->GetName());
		    StatusBar::getObjectNameLabel()->setText(name);
		    ((QWidget*)qobj)->setToolTip(documentation.c_str());
		}
            }
            
	    break;
	}
        
	case QEvent::MouseButtonDblClick:
        {
	    // Double click creates a new TCanvas with a copy of the selected object
	    if (evt->button() != Qt::LeftButton) break;
            
	    TPad* clone = (TPad*)pad->Clone();
	    clone->SetPad(0, 0, 1, 1);
            
            TQCanvas* popup = new TQCanvas();
            popup->setAttribute(Qt::WA_DeleteOnClose, true);
            popup->show();
            clone->Draw();

	    TList* list = pad->GetListOfPrimitives();
	    TIter it(list);
            QString name;
	    while (TObject* tobj = it()) {
		if (  tobj->IsA()->InheritsFrom("TH1")
                    ||tobj->IsA()->InheritsFrom("TGraph") 
                    ||tobj->IsA()->InheritsFrom("TGraph2D")
                    ||tobj->IsA()->InheritsFrom("TEfficiency"))
                {
                    name = ((TNamed*)tobj)->GetName();
                    break;
		}
            }
            popup->setWindowTitle(name);
	    break;
	}

	case QEvent::MouseButtonPress:
	    if (evt->button() == Qt::MiddleButton) {
		middleButtonPressed(qobj, evt);
	    }
            break;
                
        default:
	    break ;
    }

    return true;
}
