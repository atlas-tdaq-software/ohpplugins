/*
  File:		TableModel.cpp
  Author:	Serguei Kolos
*/

#include <iterator>

#include <ohpplugins/common/TableModel.h>

struct null_modifier
{
    template<typename T>
    void operator()(const T&)const { ; }
}; 

int ohpplugins::TableModel::Row::s_sorting_column;

QVariant 
ohpplugins::TableModel::data(const QModelIndex& index, int role) const
{
    if (index.isValid() && role == Qt::DisplayRole) {
	OrderedRows::const_iterator it = m_rows.get<1>().begin();
        std::advance(it,index.row());
	if (it != m_rows.get<1>().end()) {
	    return it->m_cells[index.column()];
	}
    }
    return QVariant();
}

QVariant 
ohpplugins::TableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    return (role == Qt::DisplayRole && orientation == Qt::Horizontal)
			? m_columns[section]
			: QVariant();
}

void 
ohpplugins::TableModel::sort(int column, Qt::SortOrder order)
{
    emit layoutAboutToBeChanged();
    Row::s_sorting_column = column;
    for(Rows::iterator it = m_rows.begin(); it != m_rows.end(); ++it) {
	m_rows.modify(it, null_modifier());
    } 
    emit layoutChanged();
}

void 
ohpplugins::TableModel::clear()
{
    beginRemoveRows(QModelIndex(), 0, m_rows.size()-1);
    m_rows.clear();
    endRemoveRows();
}

ohpplugins::TableModel::Rows::iterator 
ohpplugins::TableModel::addRow(const std::string& key)
{
    beginInsertRows(QModelIndex(), m_rows.size(), m_rows.size());
    Rows::iterator row = (m_rows.insert(Row(key,m_columns_number))).first;
    endInsertRows();
    return row;
}

void 
ohpplugins::TableModel::removeRow(const std::string& key)
{
    Rows::iterator row = m_rows.find(key);
    if (row != m_rows.end()) {
    	int index = std::distance(m_rows.get<1>().begin(),m_rows.project<1>(row));
	beginRemoveRows(QModelIndex(), index, index);
	m_rows.erase(row);
	endRemoveRows();
    }
}

ohpplugins::TableModel::Rows::iterator 
ohpplugins::TableModel::getRow(const std::string& key)
{
    Rows::iterator row = m_rows.find(key);
    if (row == m_rows.end()) {
	row = addRow(key);
    }
    return row;
}

void 
ohpplugins::TableModel::update(const std::string& histogram_name, bool show_masked)
{
    histogramUpdated(histogram_name, show_masked);
    emit dataChanged(index(0,0), index(m_columns_number, (int)m_rows.size()));
}
