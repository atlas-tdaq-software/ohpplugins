/*
  File:		TablePanel.cpp
  Author:	Serguei Kolos
*/
#include <ohpplugins/common/TablePanel.h>

#include <QClipboard>
#include <QApplication>

ohpplugins::TablePanel::TablePanel(const std::string& name)
  : PluginWidget(name, ohp::TABLE),
    m_show_masked(false)
{ 
    setupUi(this);
    
    setWindowTitle(QString::fromStdString(name));
}

void 
ohpplugins::TablePanel::on_copyToClipboardButton_clicked()
{    
    QAbstractItemModel* model = tableView->model();
    
    std::vector<int> widths(model->columnCount());
    for(int c = 0; c < model->columnCount(); ++c) {
    	widths[c] = model->headerData(c,Qt::Horizontal).toString().length();
    }
    
    for(int r = 0; r < model->rowCount(); ++r) {
	for(int c = 0; c < model->columnCount(); ++c) {
	    int l = model->data(model->index(r,c)).toString().length();
            widths[c] = std::max(widths[c],l);
	}
    }
    
    QString output;
    for(int c = 0; c < model->columnCount(); ++c) {
    	std::string h = model->headerData(c,Qt::Horizontal).toString().toStdString();
        output+= QString().asprintf(" %-*s |", widths[c], h.c_str());
    }
    output+= "\n";
    
    for(int r = 0; r < model->rowCount(); ++r) {
	for(int c = 0; c < model->columnCount(); ++c) {
	    std::string v = model->data(model->index(r,c)).toString().toStdString();
	    output+= QString().asprintf(" %-*s |", widths[c], v.c_str());
	}
	output+= "\n";
    }
    
    QApplication::clipboard()->setText(output);
}

void 
ohpplugins::TablePanel::on_showMaskedCheckbox_toggled(bool flag)
{
    m_show_masked = flag;
    updateContent();
}

void 
ohpplugins::TablePanel::updateContent()
{
    const std::vector<std::string>& histograms = getWatchedHistograms();
    for(unsigned int i = 0; i < histograms.size(); i++) {
	histogramUpdated(histograms[i].c_str());
    }
}

