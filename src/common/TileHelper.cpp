/*
 * TileHelper.cpp
 *
 *  Created on: Nov 3, 2008
 *  Author:	adotti
 *  Modified:	Serguei Kolos
 */
#include <TH1.h>

#include <ohp/CoreInterface.h>

#include <ohpplugins/common/TileHelper.h>

namespace 
{
    //ch<->PMT mapping
    const int lb_dig2pmt[48]={ 1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12,
			      13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
			      27, 26, 25, 30, 29, 28,-33,-32, 31, 36, 35, 34,
			      39, 38, 37, 42, 41, 40, 45,-44, 43, 48, 47, 46
                              };
                      
    const int eb_dig2pmt[48]={	  1, 2, 3, 4,  5,  6,  7,  8,  9, 10, 11, 12,
				 13, 14, 15, 16, 17, 18,-19,-20, 21, 22, 23, 24,
				 -27,-26,-25,-31,-32,-28, 33, 29, 30,-36,-35, 34,
				 44, 38, 37, 43, 42, 41,-45,-39,-40,-48,-47,-46
                             };
                             
    // ch<->bin name mapping
    const std::string lb_dig2name[48]={"D0_ch0","A1_ch1","B1_ch2","B1_ch3","A1_ch4","A2_ch5", //DIG8
				       "B2_ch6","B2_ch7","A2_ch8","A3_ch9","A3_ch10","B3_ch11",//DIG7
				       "B3_ch12","D1_ch13","D1_ch14","A4_ch15","B4_ch16","B4_ch17",//DIG6
				       "A4_ch18","A5_ch19","A5_ch20","B5_ch21","B5_ch22","A6_ch23",//DIG5
				       "D2_ch24","D2_ch25","A6_ch26","B6_ch27","B6_ch28","A7_ch29",//DIG4
				       "ch30","ch31","A7_ch32","B7_ch33","B7_ch34","A8_ch35",//DIG3
				       "A9_ch36","A9_ch37","A8_ch38","B8_ch39","B8_ch40","D3_ch41",//DIG2
				       "B9_ch42","ch43","D3_ch44","A10_ch45","A10_ch46","B9_ch47"//DIG1
				       };
                                       
    const std::string eb_dig2name[48]={ "E3_ch0","E4_ch1","D4_ch2","D4_ch3","B10_ch4","B10_ch5",//DIG8
					"A12_ch6","A12_ch7","B11_ch8","B11_ch9","A13_ch10",//DIG7
					"A13_ch11","E1_ch12","E2_ch13","B12_ch14","B12_ch15","D5_ch16","D5_ch17",//DIG6
					"ch18","ch19","A14_ch20","A14_ch21","B13_ch22","B13_ch23",//DIG5
					"ch24","ch25","ch26","ch27","ch28","ch29",//DIG4
					"B14_ch30","A15_ch31","A15_ch32","ch33","ch34","B14_ch35",//DIG3
					"B15_ch36","D6_ch37","D6_ch38","B15_ch39","A16_ch40","A16_ch41",//DIG2
					"ch42","ch43","ch44","ch45","ch46","ch47"//DIG1
				       };

    int
    pmt2ch(const unsigned int& pmt, const std::string modulename)
    {
	if ( pmt<1 || pmt>48 ) return -100; //ERROR!!!!
	for ( int i=0;i<=47;++i)
	{
	    int ipmt;
	    if ( modulename[0]=='L' || modulename[0]=='l' ) { ipmt = lb_dig2pmt[i]; }
	    else if ( modulename[0]=='E' || modulename[0]=='e' ) { ipmt = eb_dig2pmt[i]; }
	    else { return -100; } //ERROR!!!!
	    if ( (unsigned int)abs(ipmt) == pmt ) { return ipmt/abs(ipmt)*i; }
	}
	return -100;//ERRROR!!!!!
    }

    std::vector<unsigned int>
    dmu2ch(const unsigned int& dmu)
    {
	std::vector<unsigned int> result;
	if ( dmu > 15 ) return result;//ERROR!!!!
	result.reserve(3);
	result[0]=dmu*3;
	result[1]=result[0]+1;
	result[2]=result[0]+2;
	return result;
    }
}

ohpplugins::TileHelper::TileHelper()
{
    std::vector<std::string> histograms(4);
    histograms[0]="Histogramming/TileGATH-EF/SHIFT/Tile/Cell/tileCellStatfromDB_LBA_HG";
    histograms[1]="Histogramming/TileGATH-EF/SHIFT/Tile/Cell/tileCellStatfromDB_LBC_HG";
    histograms[2]="Histogramming/TileGATH-EF/SHIFT/Tile/Cell/tileCellStatfromDB_EBA_HG";
    histograms[3]="Histogramming/TileGATH-EF/SHIFT/Tile/Cell/tileCellStatfromDB_EBC_HG";
    initTable(histograms,m_masked_HG);

    histograms[0]="Histogramming/TileGATH-EF/SHIFT/Tile/Cell/tileCellStatfromDB_LBA_LG";
    histograms[1]="Histogramming/TileGATH-EF/SHIFT/Tile/Cell/tileCellStatfromDB_LBC_LG";
    histograms[2]="Histogramming/TileGATH-EF/SHIFT/Tile/Cell/tileCellStatfromDB_EBA_LG";
    histograms[3]="Histogramming/TileGATH-EF/SHIFT/Tile/Cell/tileCellStatfromDB_EBC_LG";
    initTable(histograms,m_masked_LG);
}

void
ohpplugins::TileHelper::initTable(	
	const std::vector<std::string>& histograms,
	std::multimap<std::string,std::string>& dict)
{
    for (std::vector<std::string>::const_iterator it = histograms.begin();
	    it!=histograms.end() ; ++it )
    {
	TH1* h = (TH1*)ohp::CoreInterface::getInstance().getHistogram(*it);
	if (!h)
	    continue;

	TAxis* xaxis = h->GetXaxis();
	TAxis* yaxis = h->GetYaxis();
	for ( Int_t xidx =xaxis->GetFirst();xidx<=xaxis->GetLast();++xidx)
	{
	    for (Int_t yidx=yaxis->GetFirst();yidx<=yaxis->GetLast();++yidx)
	    {
		if (h->GetBinContent(xidx,yidx) > 0)//A masked channel
		{
		    dict.insert(std::make_pair(xaxis->GetBinLabel(xidx), yaxis->GetBinLabel(yidx)));
		}
	    }
	}
	delete h;
    }
}

bool 
ohpplugins::TileHelper::checkChannel(
    const std::string& chname,
    const std::string& modulename,
    const Gain& gain)
{
    std::multimap<std::string,std::string>* map;
    if ( gain == HG) map=&m_masked_HG;
    else map=&m_masked_LG;
    for ( std::multimap<std::string,std::string>::const_iterator pos = map->lower_bound(modulename);
	    pos!=map->upper_bound(modulename);++pos)
    {
	if ( pos->second == chname ) return true; //Found the channel in the masked channels map, return
    }
    return false;//Channel is not in the masked channel list
}

bool 
ohpplugins::TileHelper::checkChannel(
    const unsigned int& ch,
    const std::string& modulename,
    const Gain& gain)
{
    if(modulename.empty()) return false;

    std::string chname;
    if ( ch < 48 && (modulename[0]=='L'||modulename[0]=='l'))
    { chname=lb_dig2name[ch]; }
    else if (ch < 48 && (modulename[0]=='E'||modulename[0]=='e'))
    { chname=eb_dig2name[ch]; }
    else { return false; } //ERROR cannot handle channel
    return this->checkChannel(chname,modulename,gain);
}

bool
ohpplugins::TileHelper::checkPMT(const unsigned int& pmt,const std::string& modulename)
{
    unsigned int ch = abs(pmt2ch(pmt,modulename));
    return checkChannel(ch,modulename,TileHelper::HG);
}

bool
ohpplugins::TileHelper::checkDMU(const unsigned int& dmu,const std::string& modulename)
{
    std::vector<unsigned int> chs = dmu2ch(dmu);
    bool dmuMasked = true;
    for ( std::vector<unsigned int>::const_iterator it = chs.begin(); it!=chs.end();++it)
    {
	//DMU is masked if all corresponding channels in both gains are masked
	dmuMasked &= (	   checkChannel(*it,modulename,TileHelper::HG)
			&& checkChannel(*it,modulename,TileHelper::LG) );
    }
    return dmuMasked;
}

bool
ohpplugins::TileHelper::checkCH(const std::string& chname, const std::string& modulename)
{
    return checkChannel(chname,modulename,HG);
}

bool 
ohpplugins::TileHelper::checkCH(unsigned int ch, const std::string& modulename)
{
    return checkChannel(ch,modulename,HG);
}
